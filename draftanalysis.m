%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%% ANALYZE %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%% select directory of files
%sourcefiles
OrFileDir = uigetdir;
OrFiles = dir(fullfile(OrFileDir, '*.csv')); 

%participantfiles
PPFileDir = uigetdir;
PPFiles = dir(fullfile(PPFileDir, '*.csv')); 

%%%%%%% 
for m = 1:length(PPFiles)
    
  %%%%%%%%%% ADD THE SHADOWED SOUNDFILE OF THE PP AND STORE UNDER B  
  baseFileName = PPFiles(m).name;
  fullFileName = fullfile(PPFileDir, baseFileName);
  fprintf(1, 'Now reading  %s\n', baseFileName);
  delimiter = '\t';
  formatSpec = '%*s%*s%s%*s%*s%f%f%s%*s%*s%[^\n\r]';
  fileID = fopen(fullFileName,'r');
  dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter,  'ReturnOnError', false);
  fclose(fileID);
  dataArray([2, 3]) = cellfun(@(x) num2cell(x), dataArray([2, 3]), 'UniformOutput', false);
  B = [dataArray{1:end-1}];
  
  %%%%%%%%%% FIND THE ORIGINAL SOURCEFILE EQUIVALENT OF THAT FILE
  baseFileName = (PPFiles(m).name(5:end));
  fullFileName = fullfile(OrFileDir, baseFileName);
  fprintf(1, 'Now reading the equivalent file %s\n', baseFileName);
  filename = fullFileName
  delimiter = '\t';
  formatSpec = '%*s%*s%s%*s%*s%f%f%s%*s%*s%[^\n\r]';
  fileID = fopen(fullFileName,'r');
  dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter,  'ReturnOnError', false);
  fclose(fileID);
  dataArray([2, 3]) = cellfun(@(x) num2cell(x), dataArray([2, 3]), 'UniformOutput', false);
  A = [dataArray{1:end-1}];
  
  %define ppnumber for use later on
  ppnr = PPFiles(m).name(1);
  %define filename for use later on
  vid = PPFiles(m).name(5);
  %define condition for use later on
  cond = PPFiles(m).name(3:4);
  %define PP results for later on
  res = []; 
  err = []; 
  avg = [];

  % list the words per participant
  Ba = B(:,4);
  %list the original words
  Aa = A(:,4);

%for all words spoken by our pp
for i = 1:length(Ba);
    % check what the word is 
    t = Ba{i,1};
    %correct for uhms and things you could not hear
    s1 = 'ehm';
    s2 = 'uh';
    s3 = 'xyz'
    % change to eh 
    if strcmp(t,s1) == 1 || strcmp (t,s2) == 1; 
        t = 'eh';
    end
    %count couldn't understand as an error
    if strcmp(t,s3) == 1
        C{i,1} = ppnr;
        C{i,2} = vid;
        C{i,3} = Ba{i,1};  
        C{i,4} = 999999;
    end
    %see if it matches another word in the list
    out = ismember(Aa,t);
    %if it does not match, then put a very high number in
    if max(out) == 0;
        C{i,1} = ppnr;
        C{i,2} = vid;
        C{i,3} = Ba{i,1};  
        C{i,4} = 999999;
    elseif max(out) == 1 % if it does match
        found = find(out==max(out)); % find out how often 
            for j = 1:length(found); % for all those times 
            bidx = found(j); % check out at what time it matches
            atime = A{bidx,3}; %check the original time
            btime = B{i,3}; % check the time by the PP 
            time(j) = btime - atime; %confirm the delay
                for k = 1:length(time);
                    if time(k) <= 0; %if it happens later in original file, it's not the one
                    time(k) = 999999;
                    end
                end
            end
        C{i,1} = ppnr;
        C{i,2} = vid;
        C{i,3} = Ba{i,1};  
        C{i,4} = min(time); %take the closest item
        
        time = []; %clean up time for next trial 
    end 
    
    % calculating errors also works the other way around - if the word is
    % in the source file but the pp skipped it, this should also be flagged
    
    %for all words in the original source
    for p = 1:length(Aa) 
        %check what the word is
        w = Aa{p,1};
        %see if it matches a word in the pp list
        outreverse = ismember(Ba,w);
        if max(outreverse) == 0 % if it's not in the list, it's an error
            Crev{p,1} = ppnr;
            Crev{p,2} = vid;
            Crev{p,3} = Aa{p,1}; % note down which word it is
            Crev{p,4} = 1; % note down it's an error
        elseif max(outreverse) == 1;
            foundreverse = find(outreverse==max(outreverse));
            for s = 1:length(foundreverse);
                idxw = foundreverse(s);
                distance = idxw - p; 
                if (distance < -5) && (distance > 5); 
                    Crev{p,1} = ppnr; 
                    Crev{p,2} = vid;
                    Crev{p,3} = Aa{p,1}; % note down which word it is
                    Crev{p,4} = 1; % note down it's an error%between two numbers, then say it's 0, otherwise note as error
                else
                    Crev{p,1} = ppnr;
                    Crev{p,2} = vid;
                    Crev{p,3} = Aa{p,1}; % note down which word it is
                    Crev{p,4} = 0;
                end
            end
        end
    end
end

        %calculate errors per vid
        O = cell2mat(C(1:end,4));
        ct = ismember(O,999999);
        ct1 = sum(ct);
        ct2 = sum(cell2mat(Crev(:,end)))
        sumct = ct1+ct2;
        relerror = (sumct ./ length(Aa)) * 100 %correct per file

        error{1,1} = ppnr;
        error{1,2} = vid;
        error{1,3} = cond;
        error{1,4} = sumct;
        error{1,5} = relerror;
        
        %calculate average delay per vid
        D = cell2mat(C(1:end,4));
        D(D>100) = [];

        avgdel{1,1} = ppnr
        avgdel{1,2} = vid;
        avgdel{1,3} = cond;
        avgdel{1,4} = mean(D);
  
        %save the results per trial per participant
        ppres{1,m} = vertcat(res,C);
        pperr{1,m} = vertcat(err,error);
        ppavg{1,m} = vertcat(avg,avgdel);

         clear A B C D avg avgdel err res ct sumct O error    
end  


%combine participant file
ppfinalres = vertcat(ppres{:})

%combine participant file - errors 
ppfinalerr = vertcat(pperr{:})

%combine participant file - AVG
ppfinalavg = vertcat(ppavg{:})

%this saves the .txt file for R
dlmwrite(['sub_res', num2str(ppnr),'.txt'], ppfinalres,'delimiter','\t')
dlmwrite(['sub_err', num2str(ppnr),'.txt'], ppfinalerr,'delimiter','\t')
dlmwrite(['sub_avg', num2str(ppnr),'.txt'], ppfinalerr,'delimiter','\t')

