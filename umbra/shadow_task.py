
class ShadowTask:
    """Wrapper for all required data in a task."""

    def __init__(self, participant, video, condition, source, shadow):
        self._participant = participant
        self._video = video
        self._condition = condition
        self._source = source
        self._shadow = shadow
        self._results = None
        self._delays = None

    def __str__(self):
        """Returns the String representation of shadow task data."""

        return ("participant: {} video: {} condition: {} result: {}"
                .format(self._participant,
                        self._video,
                        self._condition,
                        self._results)
                )

    def __repr__(self):
        """Returns a printable String representation of the shadow
        task data. """
        return ("{}_{}{}"
                .format(self._participant,
                        self._condition,
                        self._video)
                )

    @property
    def participant(self):
        return self._participant

    @property
    def video(self):
        return self._video

    @property
    def condition(self):
        return self._condition

    @property
    def source(self):
        return self._source

    @source.setter
    def source(self, val):
        self._source = val

    @property
    def shadow(self):
        return self._shadow

    @shadow.setter
    def shadow(self, val):
        self._shadow = val

    @property
    def results(self):
        return self._results

    @results.setter
    def results(self, results):
        self._results = results

    @property
    def delays(self):
        return self._delays

    @delays.setter
    def delays(self, delays):
        self._delays = delays
