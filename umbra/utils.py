import re
import sys


def get_path(path):
    """A function for getting the path to the
    different files of the program.

    Args:
        path: String to the path of the file.

    Returns:
        Correctly formatted path
    """
    if sys.platform.startswith('linux'):
        return 'umbra/' + path
    return path


def set_icon(window, path):
    """Set icon, depending on OS.
    'Fix' for TkInter on linux not being able to take .ico icons.

    Args:
        window: a TkInter window object.
        path: String to the .ico file.
    """
    if sys.platform.startswith('linux'):
        return
    window.iconbitmap(path)


def shadow_regex(path, video):
    """Find the reduced shadow path based on the video number.

    Args:
        path: Shadow path to check
        video: Video number

    Returns:
        participant: Participant number
        condition: Code for the condition of this video
    """
    match = re.search(f'(\d+)_(\D+){video}\.T', path.upper())
    task = None
    participant = None
    condition = None
    if match:
        participant = match.group(1)
        condition = match.group(2)
    return participant, condition


def id_regex(path):
    """Extract source file id, to use as reduced path.

    Args:
        path: Source path to find id for

    Returns:
        File id
    """
    match = re.search(r'\d+\.T', path.upper())
    if match:
        identifier = match.group()
    else:
        identifier = ''  # match is None when no such path found
    return identifier[:-2]


def add_to_dict(key, value, add_dict, alternative=False):
    """Uniquely add key, value pair to given dict.

    Args:
        key: Key to add to dictionary
        value: Corresponding value to add to dictionary
        add_dict: Dictionary to add key-value pair to
        alternative: Whether to add double keys under alternative name
            (or skip adding it)
    """
    if key not in add_dict:
        add_dict[key] = value
    else:
        if alternative:
            cnt = 1
            for k, v in add_dict.items():
                if key is k or k in key:
                    cnt += 1
            key += '({})'.format(str(cnt))
            add_dict[key] = value
        pass
