from mistake_enum import Mistake


class Word:
    """A class storing all word related data."""

    def __init__(self, word, onset, offset):
        self._word = word
        self._onset = onset
        self._offset = offset
        self._anchor = None
        self._mistake = None

    def __str__(self):
        """Returns the string representation of a word."""
        return '{} | {} | {}'.format(self._word, self._onset, self._offset)

    def __len__(self):
        """Returns the length of a word."""
        return 1

    def __eq__(self, word):
        """Compare words based on their strings alone."""
        return self._word == word.word

    @property
    def word(self):
        """Get the String of the word of this Word object.

        Returns:
            _word: String representation of the word
        """
        return self._word

    @property
    def onset(self):
        """Get the onset time of the word.

        Returns:
            _onset: a Float denoting the onset time.
        """
        return self._onset

    @property
    def offset(self):
        """Get the offset time of the word.

        Returns:
            _offset: a Float denoting the offset time.
        """
        return self._offset

    @property
    def anchor(self):
        """Get the word that this word is anchored to.

        Returns:
            _anchor: a Word object denoting the anchored word.
        """
        return self._anchor

    @property
    def mistake(self):
        """Get the mistake type of the word.

        Returns:
            _mistake: enum value denoting the mistake type.
        """
        return self._mistake

    @mistake.setter
    def mistake(self, mistake):
        """Sets the mistake type of the word.

        Args:
            mistake: enum value of the mistake type.
        """
        self._set_mistake(mistake)

    def _set_mistake(self, mistake):
        """Mistake setter. Has to be overridden in the subclass."""
        raise NotImplementedError

    def is_anchor(self):
        """Whether this word is a anchor."""
        return self._anchor is not None

    @anchor.setter
    def anchor(self, anchor):
        """Set the anchor to which this word is anchored to.

        Args:
            anchor: Word object denoting the word to be anchored to.
        """
        self._set_anchor(anchor)

    def _set_anchor(self, anchor):
        """Anchor setter. Has to be overridden in the subclass."""
        raise NotImplementedError


class ShadowWord(Word):
    """A class storing all information related to shadow words."""

    def __init__(self, word, onset, offset):
        super().__init__(word, onset, offset)
        self._correct = False
        self._source = None

    @property
    def correct(self):
        """Get whether the shadow word was correctly shadowed or not.

        Returns:
            _correct: Boolean
        """
        return self._correct

    @correct.setter
    def correct(self, value):
        """Sets whether the shadow word was correctly shadow or not.

        Args:
            value: Boolean
        """
        self._correct = value

    @property
    def source(self):
        """Get the source word of the shadow word.

        Returns:
            _source: SourceWord object
        """
        return self._source

    @source.setter
    def source(self, source):
        """Sets the source word of the shadow word.

        Args:
            source: SourceWord object
        """
        self._source = source

    def has_source(self):
        """Whether this Word is matched with a SourceWord."""
        return self._source is not None

    def _set_mistake(self, mistake):
        """Setter for the type of mistake. Only works if the word is
        not marked as correct yet and if mistake is not SKIPPED.

        Args:
            mistake: Mistake Enum
        """
        assert mistake != Mistake.SKIPPED
        self._mistake = mistake

    def _set_anchor(self, anchor):
        """Set the anchor of the shadow word.

        Args:
            anchor: Word object
        """
        self._anchor = anchor
        self._source = anchor

class SourceWord(Word):
    """A class storing all information related to source words."""

    def __init__(self, word, onset, offset):
        super().__init__(word, onset, offset)
        self._shadowed = False
        self._shadow = None

    @property
    def shadowed(self):
        """Get whether the source word was shadowed yes or no.

        Returns:
            _shadowed: Boolean
        """
        return self._shadowed

    @shadowed.setter
    def shadowed(self, value):
        """Sets whether the source word was shadowed.

        Args:
            value: Boolean
        """
        self._shadowed = value

    def _set_mistake(self, mistake):
        """Setter for the type of mistake.
        Only works if mistake is not RANDOM.

        Args:
            mistake: Mistake Enum
        """
        assert mistake != Mistake.RANDOM
        self._mistake = mistake

    def _set_anchor(self, anchor):
        """Set the anchor of the source word object.

        Args:
            anchor: Word object.
        """

        self._anchor = anchor
        self._shadow = anchor

    @property
    def shadow(self):
        """Get the shadow word object of the source word.

        Returns:
            _shadow: ShadowWord object.
        """
        return self._shadow

    @shadow.setter
    def shadow(self, shadow):
        """Sets the shadow word of the source word.

        Args:
           shadow: ShadowWord object.
        """
        self._shadow = shadow

    def has_shadow(self):
        """Whether this Word is matched with a ShadowWord."""
        return self._shadow is not None

    def get_difference(self, other):
        """Get the difference between the onset of this Word and other.

        Args:
            other: The other Word instance

        Return:
            Onset difference between this Word and other
        """
        return other.onset - self._onset


class Sentence(list):
    """A class storing all information in relation to sentences."""

    def __init__(self, words):
        super().__init__(words)

    def index(self, word):
        """Get the index of word in this Sentence.

        Args:
            word: Word to find within this Sentence

        Returns:
            index: Location of word in this Sentence,
            or -1 if word is not present
        """
        for index in range(0, len(self)):
            if self[index].onset == word.onset:
                return index
        return -1

    def find_next_matched_shadow(self, index):
        """Find the index of the next shadowed word.

        Args:
            index: Index after which to look for a matched ShadowWord

        Returns:
            index: Index of the next matched shadow
            or -1 if there is no matched shadow at of after index
        """
        if 0 <= index < len(self):
            while index < len(self):
                if self[index].has_source():
                    return index
                index += 1
        index = -1
        return index

    def find_previous_anchor(self, index):
        """Find the index of the last anchor before index.

        Args:
            index: Index before to look for an anchor

        Returns:
            anchor_index: Index of the previous anchor
            or -1 if there is no anchor at of after index
        """
        if 0 <= index < len(self):
            while index >= 0:
                if self[index].is_anchor():
                    return index
                index -= 1
        else:
            index = -1
        return index

    def find_last_matched_shadow(self, index):
        """Find the index of the last shadowed word before index.

        Args:
            index: Index before which to look for a matched ShadowWord

        Returns:
            index: Index of the previous matched shadow
            or -1 if there is no matched shadow at of after index
        """
        if 0 <= index < len(self):
            while index >= 0:
                if self[index].has_source():
                    return index
                index -= 1
        else:
            index = -1
        return index

    def __str__(self):
        return ' '.join([word.word for word in self])
