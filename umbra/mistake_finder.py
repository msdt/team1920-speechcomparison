from mistake_enum import *


class MistakeFinder:
    """Find the mistakes in already aligned lists of Words."""

    def __init__(self, seman_checker, form_checker, phon_checker):
        self._seman_checker = seman_checker
        self._form_checker = form_checker
        self._phon_checker = phon_checker
        self._source = None
        self._shadow = None

    def start(self, source, shadow):
        """Find all the mistakes and classify them.

        Args:
            source: Sentence of SourceWords
            shadow: Sentence of ShadowWords
        """
        self._source = source
        self._shadow = shadow
        # Loop over the shadow:
        for index, word in enumerate(self._shadow):
            if not word.correct and word.mistake is None:
                self._determine_mistake(index, word)
        # Loop over the source:
        for word in self._source:
            if not word.shadowed and word.mistake is None:
                # If not yet marked as mistake, then it is skipped:
                word.mistake = Mistake.SKIPPED

    def _determine_mistake(self, index, word):
        """Assign the mistake type to the given word.

        Args:
             index: Word's index in the Sentence
             word: Word to assign mistake type to
        """
        # Check for all the types of mistakes, and if it is none of them ...
        if not self._check_repetition(word, index):
            if not self._check_form_mistake(word, index):
                if not self._check_mistake(word, index, self._seman_checker,
                                           Mistake.SEMANTIC):
                    if not self._check_mistake(word, index, self._phon_checker,
                                               Mistake.PHONETIC):
                        # ... then shadow word is random:
                        word.mistake = Mistake.RANDOM

    def _check_mistake(self, shd_word, index, mistake_checker, mistake_type):
        """Check whether shd_word is a mistake according to
        mistake_checker and flag it and its source accordingly.

        Args:
            shd_word: Word to be checked for mistake
            index: Index of shd_word in shadow
            mistake_checker: Relatedness checker for two words
                Can be phonetic or semantic
            mistake_type: the type of mistake to flag

        Returns:
            mistake: Whether shd_word is a mistake according to the checker
        """
        mistake = False
        shd_anchor_index = self._shadow.find_previous_anchor(index)
        if shd_anchor_index < 0:  # If there is no anchor found,
            src_index = 0  # then just start at the beginning.
        else:
            src_index = self._source.index(
                self._shadow[shd_anchor_index].anchor)
        while (self._source[src_index].get_difference(shd_word) > 0
                       and not mistake
                       and src_index < (len(self._source) - 1)):
            src_word = self._source[src_index]
            mistake = self._related_words(src_word, shd_word, mistake_checker)
            if mistake:
                shd_word.mistake = mistake_type
                shd_word.source = src_word
                if not src_word.shadowed and src_word.mistake is None:
                    src_word.mistake = mistake_type
                if src_word.shadow is None:
                    src_word.shadow = shd_word
            src_index += 1
        return mistake

    def _check_form_mistake(self, shd_word, index):
        """Check whether shd_word is a form mistake
        and flag it and its source accordingly.

        Args:
            shd_word: Word to be checked
            index: Index of shd_word in shadow Sentence

        Returns:
            form_mistake: Whether shd_word is a form mistake
        """
        form_mistake = False
        last_shd_index = self._shadow.find_last_matched_shadow(index)
        if last_shd_index < 0:
            src_index = 0
        else:
            src_index = self._source.index(self._shadow[last_shd_index]
                                           .source)
        next_shd_index = self._shadow.find_next_matched_shadow(index)
        if next_shd_index < 0:
            src_end_index = len(self._source) - 1
        else:
            src_end_index = self._source.index(
                self._shadow[next_shd_index].source)
        while src_index <= src_end_index:
            if (self._source[src_index].get_difference(shd_word) > 0
                    and not form_mistake and src_index < len(self._source)):
                src_word = self._source[src_index]
                form_mistake = self._related_words(src_word, shd_word,
                                                  self._form_checker)
                if form_mistake:
                    shd_word.mistake = Mistake.FORM
                    shd_word.source = src_word
                    if not src_word.shadowed and src_word.mistake is None:
                        src_word.mistake = Mistake.FORM
                    if src_word.shadow is None:
                        src_word.shadow = shd_word
            src_index += 1
        return form_mistake

    def _check_repetition(self, word, index):
        """Check whether word is a repetition mistake.

        Args:
            word: Word to be checked
            index: Index of word in shadow Sentence

        Returns:
            found: Whether word is a repetition mistake
        """
        assert 0 <= index < len(self._shadow)
        found = self._check_pre_repetition(word, index)
        if not found:  # Check if word is the start of the next word.
            if (index < len(self._shadow) - 1
                    and self._shadow[index + 1].word.startswith(word.word)):
                word.mistake = Mistake.REPETITION
                found = True
        return found

    def _check_pre_repetition(self, word, index):
        """Check whether a word has a pre-repetition.

        Args:
            word: Word to be checked
            index: Index of word in shadow Sentence

        Returns:
            chain: Whether word has a pre-repetition
        """
        found_before = False
        stop = False
        diff = 0
        i = index

        while not found_before and not stop and i > 0:
            i -= 1
            stop = self._shadow[i].is_anchor()
            if self._shadow[i].word.endswith(word.word):
                found_before = True

        chain = found_before
        while chain and index - i > 1:
            diff += 1
            i += 1
            chain = self._shadow[i].word == self._shadow[index + diff].word

        if chain:
            for j in range(index, index + diff + 1):
                self._shadow[j].mistake = Mistake.REPETITION
        return chain

    @staticmethod
    def _related_words(src_word, shd_word, mistake_checker):
        """Checks if src_word and shd_word are related according to
        the specified checker.

        Args:
            src_word: SourceWord instance
            shd_word: ShadowWord instance
            mistake_checker: Checker that handles whether two words
            are related
        Returns:
            related: Whether the Words are related
        """
        shd_string = shd_word.word
        src_string = src_word.word
        related = (mistake_checker.related(src_string, shd_string)
                   and shd_string != src_string)
        return related