import pandas as pd
from utils import id_regex, shadow_regex
from shadow_task import ShadowTask
from statistics import Statistics


class Model:
    """A class for storing all related data for
    analyses on the source and shadow files."""

    def __init__(self):
        self._stats = Statistics()
        self._analysed = False
        self._shadow_tasks = []

    @property
    def analysed(self):
        return self._analysed

    @analysed.setter
    def analysed(self, value):
        self._analysed = value

    @property
    def shadow_tasks(self):
        return self._shadow_tasks

    @shadow_tasks.setter
    def shadow_tasks(self, tasks):
        self._shadow_tasks = tasks

    @property
    def stats(self):
        return self._stats

    def analysis_complete(self):
        """Check whether the analysis is completed."""
        return self._analysed

    def compare(self):
        """Run the analyses."""
        for trial in self._shadow_tasks:
            self._stats.analyze(trial)
        self._analysed = True

    def add_task(self, pnr, vnr, condition, source, shadow):
        """Add a task specifying all the necessary information.

        Args:
            pnr: Participant number
            vnr: Video number
            condition: Task of the shadowing trial
            source: Source file
            shadow: Shadow file
        """
        self._shadow_tasks.append(
            ShadowTask(pnr, vnr, condition, source, shadow))

    def remove_task(self, path, remove_all=False):
        """Remove task(s) from model.

        Args:
            path: Path to file of task to be removed
            remove_all: Whether to remove all tasks
        """
        if remove_all:
            self._shadow_tasks = []
        else:
            regex_id = id_regex(path)
            participant, condition = shadow_regex(path, regex_id)
            removes = []   # Has to do with removes in loops, this is safer
            for task in self._shadow_tasks:
                if task is not None and task.video == regex_id:
                    removes.append(True)
                elif participant is not None and condition is not None:
                    if (task.participant == participant
                            and task.condition == condition):
                        removes.append(True)
                else:
                    removes.append(False)
            for r in removes:
                if r:
                    del self._shadow_tasks[r]

    def create_delay_frame(self):
        """Create a frame for the delay information.

        Returns:
             Dataframe with delay information
        """
        delay_frame = ({'participant': [],
                        'condition': [],
                        'video': [],
                        'words': [],
                        'delays': []})
        for trial in self._shadow_tasks:
            for result in trial.delays:
                delay_frame['participant'].append(result[0])
                delay_frame['condition'].append(result[1])
                delay_frame['video'].append(result[2])
                delay_frame['words'].append(result[3])
                delay_frame['delays'].append(result[4])
        return pd.DataFrame(data=delay_frame)

    def create_mistake_frame(self):
        """Create a frame for the mistake information.

        Returns:
             DataFrame with mistake information filled in
        """
        d = ({'participant': [],
              'condition': [],
              'video': [],
              '#mistakes': [],
              'accuracy': [],
              '%phonetic': [],
              '%repetition': [],
              '%form': [],
              '%semantic': [],
              '%skipped': [],
              '%random': []})
        for trial in self._shadow_tasks:
            d['participant'].append(trial.participant)
            d['condition'].append(trial.condition)
            d['video'].append(trial.video)
            d['accuracy'].append(trial.results['accuracy'])
            d['#mistakes'].append(trial.results['#mistakes'])
            d['%phonetic'].append(trial.results['%phonetic'])
            d['%repetition'].append(trial.results['%repetition'])
            d['%form'].append(trial.results['%form'])
            d['%semantic'].append(trial.results['%semantic'])
            d['%skipped'].append(trial.results['%skipped'])
            d['%random'].append(trial.results['%random'])
        return pd.DataFrame(data=d)

    def change_algorithm(self, action):
        """Change the algorithm used for the alignment and mistake
        detection.

        Args:
            action: "anchor" for Anchor Algorithm or "nw" for
            Needleman-Wunsch
        """
        self._stats.strategy = action

    def last_used_algorithm(self):
        """Provides a string indicating which algorithm ran did the
        last alignment.

        Returns:
            String representation of the strategy
        """
        return self._stats.last_used_strategy

    @staticmethod
    def create_review_string():
        """Returns a new string for the review window."""
        return ""
