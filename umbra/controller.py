import numpy as np
import os

from filereader import CSVReader, CSVWriter
from utils import id_regex, shadow_regex
from time import localtime, strftime
from mistake_enum import *



class Controller:

    def __init__(self, view, model):
        self._model = model
        self._view = view
        self._view.action_listener = self.action_listener

        # File reading and writing supported for .csv only
        self._file_reader = CSVReader()
        self._file_writer = CSVWriter()

        # Lists of paths, represented as String objects
        self._source_files = []
        self._shadow_files = []

    def start(self):
        """Start program's user interface."""
        self._view.display()

    def action_listener(self, key):
        """Action listener for view events. Called by View class.

        Args:
            key: Identifier for functionality to execute
        """
        action, *type_code = key.split(' ')
        if action == 'select':
            self._select_files(type_code[0])
        elif action == 'delete':
            self._delete_files(type_code[0])
        elif action == 'compare':
            self._compare_files()
        elif action == 'save':
            self._save_results()
        elif action == 'save_csv':
            self._file_writer = CSVWriter()
        elif action == 'select_folder':
            self._select_folder(type_code[0])
        elif action == 'rm_all':
            self._delete_files(type_code[0], True)
        elif action == 'new_comparison':
            self._toggle_new()
        elif action == 'anchor' or action == 'nw':
            self._model.change_algorithm(action)
        elif action == 'review':
            self._review_files()
        elif action == 'save_review':
            self.save_review()
        elif action == 'cancel_review':
            self._view.close_review()
        elif action == 'combobox_review':
            self._select_review()
        elif action == 'build_tree':
            self.insert_participants()

    def _select_folder(self, type_code='shadow'):
        """Select a folder from which multiple files can be read.

        Args:
            type_code: Role of files ('source' or 'shadow')
        """
        folder_path = self._view.dir_dialog()
        if folder_path:
            file_names = os.listdir(folder_path)
            file_paths = [folder_path + '/' + x
                          for x in file_names if '.csv' in x]
            self._set_path(file_paths, type_code)
            self._view.update_file_list(file_paths, type_code)
            self._toggle_new()
        else:
            self._view.update_message('no_file')

    def _set_path(self, paths, type_code):
        """Set the file paths.

        Args:
            paths: List of file paths
            type_code: Role of files ('source' or 'shadow')
        """
        if type_code == 'source':
            self._source_files = paths
        else:
            self._shadow_files = paths

    def _select_files(self, type_code):
        """Select files and add them to the appropriate file list.

        Args:
            type_code: Role of files ('source' or 'shadow')
        """
        files = getattr(self, '_{}_files'.format(type_code))
        selection = self._view.select_files(type_code)
        files.extend(selection)
        self._view.update_file_list(files, type_code)
        self._toggle_new()

    def _delete_files(self, type_code, remove_all=False):
        """Remove the selected file from the appropriate file list.

        Args:
            type_code: Role of file ('source' or 'shadow')
            remove_all: Whether to remove all files (default: False)
        """
        selection = self._view.selected(type_code)
        files = getattr(self, '_{}_files'.format(type_code))
        if selection is not None:
            if remove_all:
                selection = [file for file in files]
                for s in selection:
                    files.remove(s)
                    self._model.remove_task('', remove_all=True)
                self._view.button_status('select {}'.format(type_code),
                                         'normal')
            else:
                selection = [file for file in files
                             if file[-len(selection) - 1].lower() == '/'
                             and selection.lower() in file.lower()][0]
                files.remove(selection)
                self._model.remove_task(selection)
                self._view.button_status('select_folder {}'.format(type_code),
                                         'normal')
            self._view.update_file_list(files, type_code)
        self._toggle_new()

    def _compare_files(self):
        """Read in and compare source and shadow data, if available."""
        self._view.reset_message()
        if not self._source_files:
            self._view.update_message('no source')
        elif not self._shadow_files:
            self._view.update_message('no shadow')
        else:
            self._toggle_new()
            successful = self._read_folder()
            if successful:
                self._view.update_message('files ok')
                self._model.compare()
                if self._model.analysis_complete:
                    self._view.update_message('comparison complete')
                    self._view.button_status('save', 'normal')
                    self._view.button_status('review', 'normal')
            else:
                self._view.update_message('no read')

    def _save_results(self):
        """Save analysis results in a file."""
        if not self._model.analysis_complete():
            self._view.update_message('no comparison')
        else:
            try:
                path = self._view.ask_save_location()
                mistake_results = self._model.create_mistake_frame()
                delay_results = self._model.create_delay_frame()
                mistake_title = self._compose_csv_title('mistakes')
                delay_title = self._compose_csv_title('delay')
                self._view.update_message('saved')
                self._file_writer.write_frame(path,
                                              mistake_results,
                                              mistake_title)
                self._file_writer.write_frame(path,
                                              delay_results,
                                              delay_title)
                self._view.update_message('saved')
            except PermissionError:
                self._view.update_message('no_dir')

    def _read_folder(self):
        """Read in multiple files from a folder.

        Returns:
            total_success: Whether reading folders was successful
        """
        source_success = []
        shadow_success = []
        non_matched_shadows = np.array([])
        non_matched_shadows = np.append(non_matched_shadows,
                                        self._shadow_files)
        for file_path in self._source_files:
            video = id_regex(file_path)
            if video is None:
                success = False
            else:
                success = True
            source_data = self._file_reader.read(file_path, 'source')
            shadows = np.array([])
            source_success.append(success)
            for shadow_candidate in non_matched_shadows:
                participant, condition = shadow_regex(shadow_candidate, video)
                if condition is not None:
                    shadow_data = self._file_reader.read(shadow_candidate,
                                                         'shadow')
                    self._model.add_task(participant,
                                         video,
                                         condition,
                                         source_data,
                                         shadow_data)
                    np.append(shadows, shadow_candidate)
                    success = True
                else:
                    success = False
                shadow_success.append(success)
            non_matched_shadows = np.delete(non_matched_shadows, shadows)
        total_success = any(shadow_success) and any(source_success)
        return total_success

    def _toggle_new(self):
        """Reset the output such that a new comparison can be made."""
        self._model.remove_task('', remove_all=True)
        self._model.analysed = False
        self._view.button_status('compare', 'normal')
        self._view.button_status('save', 'disabled')

    def _compose_csv_title(self, file_name):
        """Composes a title for a .csv file for the output of this
        program.

        Args:
            file_name: String that will be the start of the file name

        Returns:
            title: String starting with file_name, then the algorithm
            last used, then the date and lastly the time
        """
        algorithm = self._model.last_used_algorithm()
        time = strftime("%d%m%y_%H%M%S", localtime())
        title = "{}_{}_{}.csv".format(file_name, algorithm, time)
        return title

    def _review_files(self):
        """Open the result review window."""
        task_names = [str(repr(task)) for task in self._model.shadow_tasks]
        task_names.sort()
        self._view.display_review(task_names)

    def _select_review(self):
        """Change mistake in Treeview"""
        tree = self._view.review.elements['tree']
        items = tree.selection()
        for x in items:
            try:
                source, onset_src, shadow, onset_shd, mistake, review = \
                    tree.item(x)['values']
                new_review = self._view.review.elements['mistake_box'].get()
                newvals = (source,
                           onset_src,
                           shadow,
                           onset_shd,
                           mistake,
                           new_review)
                if x:
                    tree.item(x, values=newvals)
            except ValueError:
                pass

    def retrieve_participants(self, entry):
        """Get all shadow tasks corresponding to
        participant/condition/video code.

        Args:
             entry: the participant/condition/video code
        Returns:
            participants: all shadow tasks corresponding to the code.
        """
        participants = []
        for m in self._model.shadow_tasks:
            if m.participant+"_"+m.condition+m.video == entry:
                participants.append(m)
        return participants

    def reset_participants(self, participants):
        """Update mistake information according to reviewed mistakes
        by recalculating accuracy"""
        for m in self._model.shadow_tasks:
            for p in participants:
                if m.participant+"_"+m.condition+m.video ==\
                        p.participant+"_"+p.condition+p.video:
                    p.results = \
                        self._model.stats.mistake_counter.\
                        calculate_accuracy(p.source, p.shadow)

    def insert_participants(self):
        """Insert all shadow tasks/mistakes into the Treeview"""
        # Retrieve some necessary values
        tree = self._view.review.elements['tree']
        entries = self._view.review.entries
        counter = 1

        # Insert shadow tasks into the tree
        for e in entries:
            part1 = tree.insert("", counter, text="{}".format(e))
            parts = self.retrieve_participants(e)
            for participant in parts:
                # Handle source words
                for s in participant.source:
                    if s.mistake is not None:
                        source = s.word
                        onset_src = round(s.onset, 3)
                        # Add to tree
                        if s.shadow is None:
                            shadow = "NA"
                            onset_shd = "NA"
                            mistake = s.mistake
                            tree.insert(part1, "end",  text="{}".format(e),
                                        values=(source, onset_src, shadow,
                                                onset_shd, mistake.name,
                                                mistake.name))
                # Handle shadow words
                for s in participant.shadow:
                    if s.source is None:  # Without source word
                        source = "NA"
                        onset_src = "NA"
                        shadow = s.word
                        onset_shd = round(s.onset, 3)
                        mistake = s.mistake
                        # If word has a mistake, add to tree:
                        if mistake is not None:
                            tree.insert(part1, "end",  text="{}".format(e),
                                        values=(source, onset_src, shadow,
                                                onset_shd, mistake.name,
                                                mistake.name))
                    else:  # With source word
                        source = s.source.word
                        onset_src = s.source.onset
                        shadow = s.word
                        onset_shd = round(s.onset, 3)
                        mistake = s.mistake
                        # If word has a mistake, add to tree:
                        if mistake is not None:
                            tree.insert(part1, "end",  text="{}".format(e),
                                        values=(source, onset_src, shadow,
                                                onset_shd, mistake.name,
                                                mistake.name))
            counter += 1

    def save_review(self):
        """Change internal data structure to reviewed mistake"""
        # Retrieve some necessary values
        tree = self._view.review.elements['tree']
        entries = self._view.review.entries
        changed = []

        # Retrieve string mistake values from tree
        for e in entries:
            for child in tree.get_children():
                for grandchild in tree.get_children(child):
                    if tree.item(grandchild)['text'] == e:
                            src, onset, shd, onset_shd, _, new_mistake = \
                            tree.item(grandchild)['values']
                            mistake = None
                            for m in Mistake:
                            # Mistakes are strings, therefore:
                                if new_mistake.lower() == m.value:
                                    mistake = m
                            changed.append([src,
                                            onset,
                                            shd,
                                            onset_shd,
                                            mistake])

            parts = self.retrieve_participants(e)

            # Set mistakes in source to new mistake
            for participant in parts:
                for s in participant.source:
                    for c in changed:
                        if s.word == c[0] and round(s.onset, 3) == float(c[1])\
                                          and (s.shadow is None) \
                                          and c[2] == "NA":
                            new_mistake = c[4]
                            # Source mistakes cannot be random
                            if new_mistake == Mistake.RANDOM:
                                s.mistake = Mistake.SKIPPED
                            # Change to correct
                            elif new_mistake == Mistake.CORRECT:
                                s.mistake = None
                                s.shadowed = True
                            # Change to mistake
                            else:
                                s.mistake = new_mistake

                # Set mistakes in shadow to new mistake
                for s in participant.shadow:
                    for c in changed:
                        if s.word == c[2] and s.source is None \
                                          and round(s.onset, 3) == float(c[3]):
                            new_mistake = c[4]
                            # Change to correct
                            if new_mistake == Mistake.CORRECT:
                                s.mistake = None
                                s.shadowed = True
                            # Change to mistake
                            else:
                                s.mistake = new_mistake

                        elif s.source is not None:
                            if s.word == c[2] and s.source.word == c[0] \
                                              and round(s.onset, 3) \
                                                    == float(c[3]):
                                new_mistake = c[4]
                                # Mistakes for shadow words cannot be
                                # skipped, as shadows are not source words
                                if new_mistake == Mistake.RANDOM or \
                                        new_mistake == Mistake.SKIPPED:
                                    s.mistake = Mistake.RANDOM
                                    # Change mistake of source word to skipped
                                    if not s.source.shadowed:
                                        s.source.mistake = Mistake.SKIPPED
                                        s.source.shadow = None
                                    # Since the source word is skipped,
                                    # disconnect shadow from source
                                    s.source = None
                                # Change to correct
                                elif new_mistake == Mistake.CORRECT:
                                    s.mistake = None
                                    s.shadowed = True
                                # Change to mistake
                                else:
                                    s.mistake = new_mistake

            # Really change internal data structure
            self.reset_participants(parts)
            self._view.review.update_message('review saved')
