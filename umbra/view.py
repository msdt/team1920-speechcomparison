import os
import tkinter as tk
import tkinter.filedialog as fd
from functools import partial
from tkinter import ttk, W
from PIL import ImageTk, Image

from utils import set_icon, get_path, add_to_dict


class View:
    """A class for creating and displaying the GUI elements of the program."""

    def __init__(self):
        self._window = tk.Tk()

        # Center window
        self._center_window(self._window, -self._window.winfo_reqheight() / 2,
                            0, False, True)

        # Draw splash
        self._splash = SplashView(self._window)
        self._review = None

        # Create rest of GUI
        self._window.wm_title("Umbra")
        self._frame = tk.Frame(self._window).grid(column=0, row=0)
        self._elements = {}  # Dict to avoid large number of attributes
        self._selected_source = "No file selected"
        self._selected_shadow = "No file selected"
        self._action_listener = None
        self._algorithm = tk.StringVar(value='anchor')

        # Initialisation of visual and interactive elements is listed top-down
        # and left-to-right with regards to their locations in the GUI

        # Algorithm selection label/instructions
        self._create_label('alg_instructions',
                           self._frame,
                           "Select the algorithm",
                           1, 1)
        # Frame with elements for algorithm selection
        self._create_algorithm_frame()

        # File selection label/instructions
        self._create_label('file_instructions',
                           self._frame,
                           "Select files to compare",
                           4, 1)

        # Frames with elements for source and shadow
        self._create_import_frame('source')
        self._create_import_frame('shadow')

        # Compare, review and save buttons
        self._create_button('compare', self._frame, 'Compare', 7, 1)
        self._create_button('review', self._frame, 'Review results', 8, 1)
        self.button_status('review', 'disabled')
        self._create_button('save', self._frame, 'Save result', 7, 2)
        self.button_status('save', 'disabled')

        # Message
        self._create_message_frame()
        self._nr_messages = 0

        # Window Icon
        set_icon(self._window, get_path('resources/logo.ico'))

    @property
    def action_listener(self):
        return self._action_listener

    @action_listener.setter
    def action_listener(self, value):
        self._action_listener = value

    @property
    def review(self):
        return self._review

    @review.setter
    def review(self, value):
        self._review = value

    def display(self):
        """Start main loop, displaying GUI elements."""
        self._splash.terminate()
        self._window.deiconify()
        self._window.mainloop()

    @staticmethod
    def select_files(file_type):
        """Select file(s) from pc.

        Args:
            file_type: Role of file ('source' or 'shadow')

        Returns:
            paths: Path(s) to selected file(s)
        """
        dir_path = os.getcwd()
        basename = os.path.basename(dir_path)
        data_folder = dir_path.split(basename)[0] + 'data'
        if file_type == 'source':
            data_folder += r'\sourcefiles'
        else:
            data_folder += r'\shadowedfiles'
        paths = fd.askopenfilenames(
            initialdir=data_folder,  # If not found, then not a problem.
            title="Select " + file_type + " files",
            defaultextension='.csv',
            filetypes=(("Comma-separated values", '*.csv'),
                       ("TextGrid", '*.TextGrid'),
                       ("Text files", '*.txt'),
                       ("all files", '*.*')))
        return paths

    def update_file_list(self, paths, type_code):
        """Update files representation to display given list of files.

        Args:
            paths: List of file paths to be represented
            type_code: Role of files ('source' or 'shadow')
        """
        file_box = self._elements['file {}'.format(type_code)]

        if not paths:
            file_box.set("No file selected")
            file_box['values'] = []
            file_box['state'] = 'disabled'
        else:
            file_names = []
            for path in paths:
                file_names.append(path.title().split('/')[-1])
            self._set_entries_combobox(file_box, file_names)

        # Easier to just update both than to do a type check AGAIN
        self._selected_source = self._elements['file source'].get()
        self._selected_shadow = self._elements['file shadow'].get()

    @staticmethod
    def _set_entries_combobox(combobox, entries):
        """Set the entries that a combobox lists.

        Args:
            combobox: the combobox which will be updated
            entries: list with entries for the combobox
        """
        combobox['values'] = entries
        combobox.set(entries[-1])
        combobox['state'] = 'read_only'

    def reset_message(self):
        """Set the message frame label to read 'Messages'."""
        # This usually happens when the frame text reads something
        # different than 'Messages', hence the name of the method.
        self._elements['msg_frame']['text'] = 'Messages'

    def update_message(self, context=None):
        """Update message text element.

        Args:
            context: Context that the message should reflect
        """
        if context == 'no source':
            message = "Cannot compare: No source file selected!"
            self._i_am_error()
        elif context == 'no shadow':
            message = "Cannot compare: No shadow file selected!"
            self._i_am_error()
        elif context == 'files ok':
            message = "Reading..."
            self.reset_message()
        elif context == 'comparison complete':
            message = "Comparison completed!"
            self.reset_message()
        elif context == 'no comparison':
            message = "Nothing to save. Please run comparison first."
            self._i_am_error()
        elif context == 'saved':
            message = "Successfully saved file"
            self.reset_message()
        elif context == 'not_saved':
            message = "File not saved. Please try again."
            self._i_am_error(warning=True)
        elif context == 'no_file':
            message = "No file was selected."
            self._i_am_error(warning=True)
        elif context == 'no read':
            message = "Could not read in files. " \
                      "Please check the selected files."
        elif context == 'no_dir':
            message = "Please select a directory"
        else:
            message = ""
        for index in range(min(self._nr_messages, 98)):
            self._elements['message' + str(index + 1)]['text'] = \
                self._elements['message' + str(index)]['text']
        self._elements['message' + str(0)]['text'] = message
        self._nr_messages += 1
        self._window.update()

    def _perform_action(self, event, key=None):
        """Perform action for an event, based on key.

        Args:
            event: Event by which the function is called
            key: Identifier for exact event
        """
        # Keys that require some controller action
        controller_keys = [
            'select source',
            'delete source',
            'select shadow',
            'delete shadow',
            'compare',
            'save',
            'select_folder source',
            'select_folder shadow',
            'rm_all source',
            'rm_all shadow',
            'new_comparison',
            'anchor',
            'nw',
            'review',
            'build_tree',
            'combobox_review',
            'cancel_review'
        ]
        if key in controller_keys:
            self._action_listener(key)

    def ask_save_location(self):
        """Ask user for location to save file.

        Returns:
            path: Path to selected save location
        """
        path = fd.askdirectory()
        if path == '':
            self.update_message('not_saved')
        return path

    # Element creation & placement
    def _center_window(self, window, height_pad=0, width_pad=0,
                       override_redirect=False, withdraw=False):
        """Center the window.

        Args:
            window: Window to center
            height_pad: Vertical padding (default: 0)
            width_pad: Horizontal padding (default: 0)
            override_redirect: Whether to remove window borders
                (default: False)
            withdraw: Whether to withdraw the window after creation
        """
        window.update_idletasks()
        height = (window.winfo_screenheight() / 2
                  - window.winfo_reqheight())
        width = (window.winfo_screenwidth() / 2
                 - window.winfo_reqwidth())

        self._pos_right = int(width + width_pad)
        self._pos_down = int(height + height_pad)
        window.geometry('+{}+{}'.format(self._pos_right, self._pos_down))
        window.overrideredirect(override_redirect)
        window.update()
        if withdraw:
            window.withdraw()

    def _populate(self, frame):
        """Put in some fake data.

        Args:
            frame: Frame to put the data into.
        """
        for row in range(100):
            self._create_label('message' + str(row), frame, '', row, 1,
                               pad=True)

    @staticmethod
    def on_frame_configure(canvas):
        """Reset the scroll region to encompass the inner frame.

        Args:
            canvas: Canvas to reset scroll region of
        """
        canvas.configure(scrollregion=canvas.bbox('all'))

    def _create_message_frame(self):
        """Create the frame in which to display messages."""
        frame = tk.LabelFrame(self._frame, text='Messages', width=200,
                              height=80)
        frame.grid(column=1, row=9, padx=15, pady=10,
                   columnspan=2, sticky='NESW')
        canvas = tk.Canvas(frame, borderwidth=0, height=80)
        inner_frame = tk.Frame(canvas, height=80)
        vsb = tk.Scrollbar(frame, orient='vertical', command=canvas.yview)
        canvas.configure(yscrollcommand=vsb.set)

        vsb.pack(side='right', fill='y')
        canvas.pack(side='left', fill='both', expand=True)
        canvas.create_window((4, 4), window=inner_frame, anchor='nw')

        inner_frame.bind('<Configure>',
                         lambda event,
                         canvas=canvas: self.on_frame_configure(canvas))
        self._populate(inner_frame)
        add_to_dict('msg_frame', frame, self._elements)

    def _i_am_error(self, warning=False):
        """Set message label to WARNING or ERROR dependent on situation.

        Args:
            warning: Whether label is to be WARNING (or ERROR)
        """
        if warning:
            self._elements['msg_frame']['text'] = 'WARNING'
        else:
            self._elements['msg_frame']['text'] = 'ERROR'

    def _create_import_frame(self, type_code):
        """Create frame for file selection for given type.

        Args:
            type_code: Role of frame ('source' or 'shadow')
        """
        if type_code == 'source':
            frame_title = "Source: "
            column = 1
        else:
            frame_title = "Shadow: "
            column = 2

        # Create frame
        frame = tk.LabelFrame(self._frame, text=frame_title)
        frame.grid(column=column, row=5, padx=15, pady=10)

        # Interactive elements
        self._create_combobox('file {}'.format(type_code),
                              frame,
                              2, column)
        self._create_button('select {}'.format(type_code),
                            frame,
                            'Add {} file'.format(type_code),
                            5, column)
        self._create_button('delete {}'.format(type_code),
                            frame,
                            'Remove {}'.format(type_code),
                            7, column)
        self._create_button('rm_all {}'.format(type_code),
                            frame,
                            'Clear {} files'.format(type_code),
                            8, column)
        self._create_button('select_folder {}'.format(type_code),
                            frame,
                            'Add {} folder'.format(type_code),
                            6, column)

    def _create_algorithm_frame(self):
        """Create frame for the algorithm selection."""

        # Create frame
        frame = tk.LabelFrame(self._frame)
        frame.grid(column=1, row=2, padx=24, pady=10, columnspan=2, sticky=W)

        # Radio buttons for algorithm selection
        self._create_radio('anchor',
                           frame,
                           "Anchor (fast)",
                           self._algorithm,
                           2, 1)
        self._create_radio('nw',
                           frame,
                           "Needleman-Wunsch (slow)",
                           self._algorithm,
                           3, 1)

    def _create_button(self, key, frame, text, row, column, sticky=''):
        """Create button and place it in the given frame.

        Args:
            key: Key to save button in _elements dictionary
            frame: Frame into which to place the button
            text: Text on the button
            column: Column in grid to place the button in
            row: Row in grid to place the button in

        Raises:
            ValueError: Key already exists
        """
        button = tk.Button(frame, text=text, width=17)
        button.grid(column=column, row=row, padx=5, pady=10, sticky=sticky)

        action_function = partial(self._perform_action, key=key)
        button['command'] = lambda: action_function('<Button>')

        add_to_dict(key, button, self._elements)

    def _create_radio(self, key, frame, text, var, row, column):
        """Create radio button and place it in the given frame.

        Args:
            key: Key to save radio button in _elements dictionary
            frame: Frame into which to place the radio button
            text: Text on the radio button
            column: Column in grid to place the radio button in
            row: Row in grid to place the radio button in
            var: Common variable that is changed by radio button

        Raises:
            ValueError: Key already exists
        """
        radio = tk.Radiobutton(frame, text=text, width=22, variable=var,
                               value=key, anchor=W)
        radio.grid(column=column, row=row, padx=5, pady=1, sticky=W)

        action_function = partial(self._perform_action, key=key)
        radio.bind('<Button>', action_function)

        add_to_dict(key, radio, self._elements)

    def _create_combobox(self, key, frame, row, column, sticky='W'):
        """Create combobox, a drop-down file selection widget.

        Args:
            key: Key to save button in _elements dictionary
            frame: Frame into which to place the button
            column: Column in grid to place the button in

        Raises:
            ValueError: Key already exists
        """
        file_box = ttk.Combobox(frame,
                                state='disabled',
                                width=20)
        file_box.set("No file selected")

        file_box.grid(column=column, row=row, sticky=sticky, padx=10, pady=10)

        action_function = partial(self._perform_action, key=key)
        file_box.bind('<Button>', action_function)

        add_to_dict(key, file_box, self._elements)

    def _create_label(self, key, frame, text, row, column,
                      pad=False, sticky=''):
        """Create textual label and place it in the given frame.

        Args:
            key: Key to save label in _elements dictionary
            frame: Frame into which to place the label
            text: Text of the label
            row: Row in grid to place the label in

        Raises:
            ValueError: Key already exists
        """
        label = tk.Label(frame, text=text)
        label.grid(column=column, row=row, sticky=sticky)
        if pad:
            label.grid(column=column, row=row, pady=10, padx=10, sticky=sticky)
        add_to_dict(key, label, self._elements)

    def button_status(self, key, mode):
        """Change button corresponding to key to given mode.

        Args:
            key: Key of button to change
            mode: What mode to change the button to
        """
        button = self._elements[key]
        button['state'] = mode
        if mode == 'disabled':
            button.unbind('<Button>')
        elif mode == 'normal':
            action_function = partial(self._perform_action, key=key)
            button['command'] = lambda: action_function('<Button>')

    def dir_dialog(self):
        """Open up a file dialog window.

        Returns:
            selection: String object denoting the selected directory
        """
        selection = fd.askdirectory()
        if selection:
            return selection
        else:
            self.update_message('no_dir')

    def selected(self, type_code):
        """Get selected files.

        Args:
            type_code: Role of files ('source' or 'shadow')

        Returns:
            selected: Files selected
        """
        selected = self._elements['file {}'.format(type_code)].get()
        if selected == "No file selected" or not selected:
            return None
        else:
            return selected

    def display_review(self, task_names):
        """Displays a pop-up window for reviewing the results of
        the analysis."""
        self._review = ReviewWindow(self._window, task_names)
        self._review.action_listener = self.action_listener
        self._review.create_tree()
        self._review.update_mistake_combobox()

    def close_review(self):
        """Close review window."""
        self._review.withdraw()


class SplashView(tk.Toplevel, View):
    """A splash screen while the rest of the application is loading."""

    def __init__(self, parent):
        tk.Toplevel.__init__(self, parent)
        self.grab_set()
        self.title("Umbra")
        set_icon(self, get_path("resources/logo.ico"))
        im_path = Image.open(get_path("resources/splash.png"))
        ph = ImageTk.PhotoImage(im_path)
        load_img = tk.Label(self, image=ph)
        load_img.grid(row=0, column=0)

        self._center_window(self, ph.height() / 2, ph.width() / 2,
                            True, False)

    def terminate(self):
        """Close the splash window."""
        self.destroy()


class ReviewWindow(tk.Toplevel, View):
    """A class for displaying a review window in which analysis
     results can be manually changed. """

    def __init__(self, parent, names):
        tk.Toplevel.__init__(self, parent)
        self.title("Review results")
        set_icon(self, get_path('resources/logo.ico'))
        self._entries = names
        self._elements = {}  # Dict to avoid large number of attributes
        self._action_listener = None

        # Create frames/window
        self._file_frame = tk.Frame(self)
        self._file_frame.grid(row=1, column=0, padx=10, sticky='W')
        self._msg_frame = tk.Frame(self)
        self._msg_frame.grid(row=4, column=0, padx=10, sticky='W')
        self._bt_frame = tk.Frame(self)
        self._create_message_frame()
        self._center_window(self)

        # Create labels
        self._create_label('review_title',
                           self,
                           "Review marked mistakes by " +
                           "clicking on a mistake " +
                           "\n and selecting the new mistake " +
                           "in the dropdown.",
                           0, 0, False, 'N')
        self._elements['review_title'].grid(columnspan=2)
        self._create_label('mistake_label', self._file_frame,
                           'Mistake: ', 1, 0, True, 'W')

        # Create mistake combobox
        self._create_combobox('mistake_box', self._file_frame,
                              1, 1)
        self.update_mistake_combobox()
        # Create buttons
        self._create_button('save_review', self._bt_frame, 'Save', 3, 0, 'W')
        self._create_button('cancel_review', self._bt_frame, 'Quit', 3, 1,
                            'W')
        self._bt_frame.grid(row=3, column=0, padx=10, sticky='W')

    def _perform_action(self, event, key=None):
        """Perform action for an event, based on key.

        Args:
            event: Event by which the function is called (unused, but
            framework requires it)
            key: Identifier for exact event.
        """
        controller_keys = [
                'review',
                'build_tree',
                'save_review',
                'combobox_review',
                'cancel_review'
            ]
        if key in controller_keys:
            self._action_listener(key)

    def update_mistake_combobox(self):
        """Update the combobox containing the mistakes with the functionality and
        mistake options. """
        action_function = partial(self._perform_action, key="combobox_review")
        self._elements['mistake_box'].bind("<<ComboboxSelected>>",
                                           action_function)
        mistakes = ["CORRECT", "FORM", "PHONETIC", "RANDOM", "REPETITION",
                    "SEMANTIC", "SKIPPED"
                    ]
        self._set_entries_combobox(self._elements['mistake_box'], mistakes)

    def create_tree(self):
        """Create frame object containing the tree object."""
        wordlist_frame = tk.Frame(self)
        add_to_dict("frame", wordlist_frame, self._elements)
        tree = ttk.Treeview(wordlist_frame)
        add_to_dict("tree", tree, self._elements)
        self._build_tree(2, 0)

    def _build_tree(self, row, column):
        """Create the tree elements in the review window."""
        tree = self._elements['tree']
        word_list_frame = self._elements['frame']
        ysb = ttk.Scrollbar(word_list_frame, orient='vertical',
                            command=tree.yview)
        tree.configure(yscroll=ysb.set)
        names = ["Participant", "Source", "Onset", "Shadow", "Onset",
                 "Mistake", "Review"]
        headers = ("#0", "ptc", "src", "ons", "shd", "ons2", "mis", "rev")
        tree["columns"] = headers
        for i in range(len(names)):
            tree.heading(headers[i], text=names[i], anchor=tk.W)
        self.action_listener('build_tree')
        ysb.pack(side='right', fill='y')
        tree.pack(side=tk.TOP, fill=tk.X)
        word_list_frame.grid(row=row, column=column, columnspan=2, padx=10)

    def _create_message_frame(self):
        """Create the frame in which to dispglay messages."""
        frame = tk.LabelFrame(self._msg_frame, text='Messages', width=300,
                              height=30)
        frame.grid(column=0, row=4, padx=15, pady=10, sticky='E')
        self._create_label('review_msg', frame,
                           '\t\t\t\t\t', 1, 0, True, 'W')
        add_to_dict('review_msg_frame', frame, self._elements)

    def update_message(self, context=None):
        """Update message text element.

        Args:
            context: Context that the message should reflect
        """
        if context == 'review saved':
            message = "Review saved successfully"
        else:
            message = "Review failed"
        self._elements['review_msg']['text'] = message

    @property
    def action_listener(self):
        return self._action_listener

    @action_listener.setter
    def action_listener(self, value):
        self._action_listener = value

    @property
    def elements(self):
        return self._elements

    @elements.setter
    def elements(self, value):
        self._elements = value

    @property
    def entries(self):
        return self._entries
