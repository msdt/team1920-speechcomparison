from abc import ABC, abstractmethod


class AlignmentStrategy(ABC):

    @abstractmethod
    def __str__(self):
        """Provide a String representation of the AlignmentStrategy."""
        pass

    @abstractmethod
    def align(self, source, shadow):
        """Align a pair of source and shadow Sentences.

        Args:
            source: The Sentence of SourceWords
            shadow: The Sentence of ShadowWords
        """
        pass
