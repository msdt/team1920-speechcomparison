from alignment_strategy import AlignmentStrategy


class AnchorAlgorithm(AlignmentStrategy):
    """Class that aligns the words of a source and a shadow Sentence using
    the anchor algorithm."""

    def __init__(self, mistake_finder):
        super().__init__()
        self._source = None
        self._shadow = None
        self._mistake_finder = mistake_finder

    def __str__(self):
        """Provide a String that represents the anchor algorithm.

        Returns:
            The String literal "anchor"
        """
        return "anchor"

    def align(self, source, shadow):
        """Align a pair of source and shadow Sentences using the
        anchor algorithm.

        Args:
            source: The Sentence of SourceWords
            shadow: The Sentence of SourceWords
        """
        self._source = source
        self._shadow = shadow
        self._find_anchors()
        self._compare()
        self._mistake_finder.start(self._source, self._shadow)

    def _compare(self):
        """Search the source and shadow Sentences for correctly shadowed
        Words between the anchor Word pairs.
        """
        shd_last_anchor = 0
        src_last_anchor = 0
        for src_index, word in enumerate(self._source):
            if word.is_anchor():
                # If there are non-anchor words before the anchor,
                # try to shadow them
                if src_last_anchor < src_index:
                    shd_index = self._shadow.index(word.anchor)
                    self._search_between_anchors(src_last_anchor,
                                                 shd_last_anchor,
                                                 src_index,
                                                 shd_index)
                shd_last_anchor = self._shadow.index(word.anchor) + 1
                src_last_anchor = src_index + 1
            # If the last word is not an anchor, then still search for
            # matches:
            elif src_index == len(self._source) - 1:
                src_index += 1
                shd_index = len(self._shadow)
                self._search_between_anchors(src_last_anchor,
                                             shd_last_anchor,
                                             src_index,
                                             shd_index)

    def _search_between_anchors(self, src_start, shd_start, src_end, shd_end):
        """Search for shadowed Words in an interval between two Word pairs.

        Args:
            src_start: Starting index of source interval
            shd_start: Starting index of shadow interval
            src_end: Ending index of source interval
            shd_end: Ending index of shadow interval
        """
        for src_index in range(src_start, src_end):
            src_word = self._source[src_index]
            if not src_word.is_anchor():
                self._search_word_in_interval(src_word, shd_start, shd_end)
            else:
                # src_word is an anchor, only search in shadow after it
                shd_start = self._shadow.index(src_word.anchor())

    def _search_word_in_interval(self, src_word, shd_start, shd_end):
        """Search for the shadow match of a SourceWord within an interval
        and flag both Words if a shadow is found.

        Args:
            src_word: SourceWord for which shadow must be found
            shd_start: Starting index of shadow interval
            shd_end: End index of shadow interval
        """
        found = False
        competing_over = None
        for shadow_index in range(shd_start, shd_end):
            shd_word = self._shadow[shadow_index]
            if src_word.word == shd_word.word and not found:
                if 0.05 < src_word.get_difference(shd_word) < 3.0:
                    if not shd_word.correct:
                        found = True
                        competing_over = None
                        src_word.shadowed = True
                        src_word.shadow = shd_word
                        shd_word.correct = True
                        shd_word.source = src_word
                    else:
                        # If potential shadow is already flagged,
                        # store it
                        competing_over = shd_word
        # If true, then competing_over was wrongly flagged as correct:
        if competing_over is not None and competing_over.source is not None:
            competing_over.source.shadowed = False
            competing_over.source.shadow = None
            competing_over.source = src_word
            src_word.shadowed = True

    def _find_anchors(self):
        """Search for shadowed Words that are unique in both the source and
        the shadow Sentence. Mark them as anchors iff the shadow occurs
        shortly after the SourceWord onset.
        """
        # Find the words that are unique in the source:
        unique_in_src = self._find_unique_source_words()

        # See if those unique source words are shadowed:
        possible_anchors = self._find_unique_shadow_mirrors(unique_in_src)

        # Set all pairs to correctly shadowed and anchor them:
        for value in possible_anchors.values():
            if len(value) == 2:  # Check if it is a pair, not a single
                src_word = value[0]
                shd_word = value[1]
                length = len(src_word.word)
                if length >= 3:  # Do not anchor shorter words
                    if length > 5:  # Long words can have 3s onset diff
                        max_onset_difference = 3
                    else:  # Be more strict for shorter words
                        max_onset_difference = 1.5
                    if (0.05 < src_word.get_difference(shd_word)
                            < max_onset_difference):
                        shd_word.anchor = src_word
                        shd_word.correct = True
                        src_word.anchor = shd_word
                        src_word.shadowed = True

    def _find_unique_source_words(self):
        """Find all words that occur exactly once in the source.

        Returns:
            uniques: Dictionary of unique source word Strings and the
            Word objects in which they occur.
        """
        uniques = {}
        not_unique_keys = set()

        for word in self._source:
            word_str = word.word
            if word_str not in uniques:  # First occurrence is unique
                uniques[word_str] = word
            else:  # If already in uniques, then it is not unique
                not_unique_keys.add(word_str)

        for key in not_unique_keys:  # Delete not unique keys
            del uniques[key]
        return uniques

    def _find_unique_shadow_mirrors(self, src_uniques):
        """Find the shadows of unique source words and make sure they
        are unique as well.

        Args:
            src_uniques: Unique source words and their Word objects

        Returns:
            uniques: Unique pairs of source and shadow words
        """
        uniques = src_uniques
        for word in self._shadow:  # Loop over shadow words
            word_str = word.word
            if word_str in uniques:  # If word is a unique source word:
                # ... remove if already found
                if len(uniques[word_str]) == 2:
                    uniques.pop(word_str)
                else:  # ... or make a list of the unique words
                    uniques[word_str] = [uniques[word_str], word]
        return uniques
