from bisect import bisect_left
from OpenDutchWordnet.le import Le
from OpenDutchWordnet.synset import Synset
from OpenDutchWordnet.wn_grid_parser import Wn_grid_parser
from utils import get_path


class SemanticChecker:
    """A class for checking semantic mistakes in the data."""

    def __init__(self, parser=None):
        path = 'OpenDutchWordnet/resources/odwn/odwn_orbn_gwg-LMF_1.3.xml.gz'
        if parser:
            self._parser = parser
        else:
            self._parser = Wn_grid_parser(get_path(path))
            self._parser.load_synonyms_dicts()
        self._lemma2synsets = getattr(self._parser, 'lemma2synsets')

        le_frame = sorted(self._create_les_frame(),
                          key=lambda kv: kv.get_lemma())
        self._lemma_frame = [entry.get_lemma() for entry in le_frame]

        reltypes = getattr(self._parser, 'reltypes')
        syn_ids = getattr(self._parser, 'syn_ids')
        self._synset_frame = sorted(self._create_synset_frame(syn_ids,
                                                              reltypes),
                                    key=lambda kv: kv.get_id())
        self._syn_id_frame = [syn.get_id() for syn in self._synset_frame]

    def _create_les_frame(self):
        """Create a list of instances of the Le (Lexical entry) class.

        Returns:
            List of instances of the Le class.
        """
        le_list = []
        for lex_elem in self._parser.doc.iterfind(self._parser.path_to_le_els):
            instance = Le(lex_elem, self._parser.lexicon_el)
            le_id = instance.get_id()
            if 'mwe' not in le_id:
                le_list.append(instance)
        return le_list

    def _create_synset_frame(self, syn_ids, reltypes):
        """Create a list of instances of the Synset class.

        Args:
            syn_ids: List of Synset ids
            reltypes: # TODO: add description of parameter reltypes

        Returns:
            List of instances of the Synset class
        """
        return [Synset(synset_elem, reltypes, syn_ids)
                for synset_elem
                in self._parser.doc.iterfind(self._parser.path_to_synset_els)]

    def _find_synsets(self, ids):
        """Find the Synsets corresponding to the given ids.

        Args:
            ids: List of Synset ids

        Returns:
            List of synsets corresponding to the given ids
        """
        return [self._find_synset(syn_id) for syn_id in ids
                if syn_id is not None]

    def _find_synset(self, syn_id):
        """Find a Synset based on a Synset id.

        Args:
            syn_id: Synset id

        Returns:
             Synset corresponding to the given Synset id
        """
        i = bisect_left(self._syn_id_frame, syn_id)
        return self._synset_frame[i]

    def related(self, source_word, shadow_word):
        """Check whether the given words are semantically related.

        Args:
            source_word: Source string
            shadow_word: Shadow string

        Returns:
            Whether the words are assemantically related
        """
        if not self._is_unknown(source_word, shadow_word):
            return (self._are_synonyms(source_word, shadow_word)
                    or self._hypernym_related(source_word, shadow_word))
        return False

    # Stuff for unknown check
    def _is_unknown(self, source, shadow):
        """Check whether the given words are unknown in our dataset.

        Args:
            source: Source string
            shadow: Shadow string

        Returns:
            Whether either input word is unknown
        """
        return not (self._is_known(source) and self._is_known(shadow))

    def _is_known(self, lemma):
        """Check whether lemma is known in the database.

        Args:
             lemma: word to check for

        Returns:
            Whether lemma is known in the database
        """
        i = bisect_left(self._lemma_frame, lemma)
        return i != len(self._lemma_frame) and self._lemma_frame[i] == lemma

    # Functions related to synonym-checking
    def _are_synonyms(self, source, shadow):
        """Check whether the given words are synonyms.

        Args:
            source: Source string
            shadow: Shadow string

        Returns:
            Whether the words are synonyms
        """
        return shadow in self._parser.lemma_synonyms(source)

    # Functions related to hypernym checking
    def _hypernym_related(self, source, shadow):
        """Check whether the input words are hypernyms.

        Args:
            source: Source string
            shadow: Shadow string

        Returns:
            Whether the words are hypernyms
        """
        source_hypernyms = self._get_hypernyms(source, 1)
        shadow_hypernyms = self._get_hypernyms(shadow, 1)
        source_hypernyms2 = [syn.get_id() for syn in source_hypernyms]
        shadow_hypernyms2 = [syn.get_id() for syn in shadow_hypernyms]
        for hypernym in shadow_hypernyms2:
            if hypernym in source_hypernyms2:
                return True
        return False

    def _get_hypernyms(self, lemma, depth):
        """Find the hypernyms associated with lemma
        until depth in the tree.

        Args:
            lemma: word to find hypernyms for
            depth: Maximum depth to look at

        Returns:
            All found hypernyms
        """
        original_syn_ids = self._lemma2synsets[lemma]
        original_synsets = self._find_synsets(original_syn_ids)
        hypernyms = set()
        return self._hypernym_helper(hypernyms, original_synsets, depth, 0)

    def _hypernym_helper(self, hypernyms, synsets, depth, current_depth):
        """Helper function for the recursive get_hypernyms().

        Args:
            hypernyms: Set of hypernyms
            synsets: Set of hypernyms to be added
            depth: Maximum depth to look at
            current_depth: Current depth

        Returns:
            All found hypernyms
        """
        hypernyms.update(synsets)
        if current_depth == depth:
            return hypernyms
        else:
            all_relations = set()
            for synset in synsets:
                target_ids = [rel.get_target() for rel in
                              synset.get_relations('has_hyperonym')]
                target_synsets = self._find_synsets(target_ids)
                all_relations.update(target_synsets)
            return self._hypernym_helper(hypernyms,
                                         all_relations,
                                         depth,
                                         current_depth + 1)
