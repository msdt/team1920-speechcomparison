from enum import Enum


class NoValue(Enum):
    """Value is not important in this class."""
    def __repr__(self):
        """Hide the (unimportant) value, when a NoValue Enum is printed."""
        return '<{}.{}>'.format(self.__class__.__name__, self.name)


class Mistake(Enum):
    """All types of mistakes that can be made in the shadowing task."""
    PHONETIC = 'phonetic'
    REPETITION = 'repetition'  # For repetitions of words or word parts
    SEMANTIC = 'semantic'
    SKIPPED = 'skipped'  # For source words that are not shadowed
    RANDOM = 'random'  # For shadow words that do not reflect a source word
    FORM = 'form'   # For verbs that are shadowed in another form
    CORRECT = 'correct'
