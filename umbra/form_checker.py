import pandas as pd
from bisect import bisect_left
from utils import get_path


class FormChecker:

    def __init__(self):
        self._initialize_ndarrays()
        self._prefixes = ['', 'ge', 'be', 'ver', 'on', 'ont']
        self._affixes = ['', 'en', 't', 'te', 'ten', 'de', 'den', 's', "'s"]
        self._verb_prefixes = ['ge', 'ver', 'be', 'ont', 'her', 'mis']
        self._verb_affixes = ['den', 'ten', 'de', 'te', 'en', 't', 'd', 'n',
                              '']
        self._consonants = ['b', 'd', 'f', 'g', 'k', 'l', 'm', 'n', 'p', 'r',
                            's', 't', 'z', 'c', 'h', 'j', 'q', 'v', 'w', 'x']
        self._vowels = ['a', 'e', 'o', 'u']

    def _initialize_ndarrays(self):
        """Initialize numpy arrays for irregular verbs."""
        irr_verbs_frame = pd.read_csv(get_path(
            'resources/irregular_verbs.csv'))
        self._irr_verbs_vte = self._create_ndarray(irr_verbs_frame,
                                                   'verleden tijd enkelvoud')
        self._irr_verbs_vtm = self._create_ndarray(irr_verbs_frame,
                                                   'verleden tijd meervoud')
        self._irr_verbs_perfect = self._create_ndarray(irr_verbs_frame,
                                                       'voltooid deelwoord')
        self._irr_verbs = irr_verbs_frame.values

    @staticmethod
    def _create_ndarray(df, key):
        """Create a new numpy array based on given DataFrame and key.

        Args:
            df: pandas DataFrame that the ndarray is based on
            key: Which column to sort the dataframe on

        Returns:
            A new dataframe sorted on the specified column
        """
        return df.copy().sort_values(key).values

    def related(self, source_word, shadow_word):
        """Check whether the given Words are form-related.

        Args:
            source_word: Source string
            shadow_word: Shadow string

        Returns:
            Whether the words are form-related
        """
        return (self._fix_related(source_word, shadow_word)
                or self._irr_verb_related(source_word, shadow_word)
                or self._reg_verb_related(source_word, shadow_word))

    def _irr_verb_related(self, source_word, shadow_word):
        """Check whether the given Words are different versions of
        the same irregular verb.

        Args:
            source_word: Source string
            shadow_word: Shadow string

        Returns:
            Whether the words are different versions
            of the same irregular verb.
        """
        return any(self._irr_verb_helper(self._irr_verbs,
                                         key,
                                         source_word,
                                         shadow_word) for key in range(4))

    def _irr_verb_helper(self, frame, key, source_word, shadow_word):
        """Check if the given Words are in the same row.

        Args:
            frame: Array of Strings
            key: Which column number to look in
            source_word: Source string
            shadow_word: Shadow string

        Returns:
            Whether the source and shadow words are in the same row.
        """
        i = bisect_left(frame[:, key], source_word)
        return i != len(frame[:, key]) and shadow_word in frame[i]

    def _reg_verb_related(self, source_word, shadow_word):
        """Check whether the given Words are different conjugations of
        the same verb.

        Args:
            source_word: Source string
            shadow_word: Shadow string

        Returns:
            Whether the words are different conjugations
            of the same verb.
        """
        return (self._reg_verb_helper(source_word, shadow_word)
                or self._reg_verb_helper(shadow_word, source_word))

    def _reg_verb_helper(self, word1, word2):
        """Check whether the given Words are different conjugations of
        the same verb.
        One-directional helper function for _reg_verb_related

        Args:
            word1: Source string
            word2: Shadow string

        Returns:
            Whether the word1 and word2 are different conjugations
            of the same verb.
        """
        stem = self._get_stem(word1)
        perfect1, perfect2 = self._create_perfect(stem)
        return (perfect1 == word2
                or perfect2 == word2
                or any(stem + affix == word2 for affix in self._verb_affixes))

    def _create_perfect(self, stem):
        """Create the perfect time form of the verb based on the stem.

        Args:
            stem: Stem of a word

        Returns:
            word_1: Perfect time form with 'd'
            word_2: Perfect time form with 't'
        """
        word = 'ge' + stem
        for v_prefix in self._verb_prefixes:
            if (stem[:len(v_prefix)] == v_prefix
                    and len(stem) > len(v_prefix) + 1):
                word = word[2:]
        word_1 = word + 'd'
        word_2 = word + 't'
        return word_1, word_2

    def _get_stem(self, word):
        """Find the stem of the given Word.

        Args:
            word: String

        Returns:
            stem: Stem of the given word
        """
        stem = ''
        for affix in self._verb_affixes:
            length = len(affix)
            if ((word[-length:] == affix or affix == '')
                    and not self._is_vdw(word)):
                if affix == '' or len(word) == length:
                    stem = word
                else:
                    stem = word[:-length]
                stem = self._create_single_stem(stem, affix)
                return stem
            elif word[-length:] == affix:
                stem = word[2:-length]
                return stem
        return stem

    def _is_vdw(self, word):
        """Check whether the input word is in perfect time form.

        Args:
            word: String

        Returns:
            Whether the given word is in perfect time form.
        """
        return word[:2] == 'ge' and (word[-1] == 't' or word[-1] == 'd')

    def _create_single_stem(self, stem, affix):
        """Find the single-person stem based on the stem and affix.

        Args:
            stem: The stem of a word
            affix: The affix used to create the stem

        Returns:
            stem: Single-person stem of the given stem
        """
        if affix == 'en' and len(stem) > 2 and stem != affix:
            if stem[-1] in self._consonants and stem[-1] == stem[-2]:
                stem = stem[:-1]
            elif (stem[-1] in self._consonants
                    and stem[-2] in self._vowels
                    and stem[-3] in self._consonants):
                stem = stem[:-1] + stem[-2:]
        if stem[-1] == 'z':
            return stem[:-1] + 's'
        elif stem[-1] == 'v':
            return stem[:-1] + 'f'
        return stem

    def _fix_related(self, source_word, shadow_word):
        """Check whether the arguments are related in terms of their
        prefixes and affixes.

        Args:
            source_word: Source string
            shadow_word: Shadow string

        Returns:
            Whether the arguments are related in terms of
            their prefixes and affixes.
        """
        return (self._prefix_related(source_word, shadow_word)
                or self._affix_related(source_word, shadow_word)
                or self._prefix_related(shadow_word,source_word)
                or self._affix_related(shadow_word,source_word))

    def _prefix_related(self, source_word, shadow_word):
        """Check whether the arguments are related in terms of their
        prefixes.

        Args:
            source_word: Source string
            shadow_word: Shadow string

        Returns:
            Whether the arguments are related in terms of
            their prefixes.
        """
        for pre in self._prefixes:
            len_pre = len(pre)
            if shadow_word[:len_pre] == pre:
                shadow_rest = shadow_word[len_pre:]
                for pre2 in self._prefixes:
                    if pre2 + shadow_rest == source_word:
                        return True
        return False

    def _affix_related(self, source_word, shadow_word):
        """Check whether the arguments are related in terms of their
        affixes.

        Args:
            source_word: Source string
            shadow_word: Shadow string

        Returns:
            Whether the arguments are related in terms of
            their affixes.
        """
        for aff in self._affixes:
            len_aff = len(aff)
            if shadow_word[-len_aff:] == aff:
                shadow_rest = shadow_word[:-len_aff]
                for aff2 in self._affixes:
                    if shadow_rest + aff2 == source_word:
                        return True
                    elif self._prefix_related(shadow_rest + aff2,
                                              source_word):
                        return True
        return False
