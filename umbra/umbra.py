from controller import Controller
from model import Model
from view import View


class Umbra:
    """Main class of the program for initializing the different
    components."""

    def __init__(self):
        self._view = View()
        self._model = Model()
        self._controller = Controller(self._view, self._model)

    def main(self):
        """Start the main loop."""
        self._controller.start()


if __name__ == '__main__':
    """Start of the program."""
    Umbra().main()
