import pandas as pd
from abc import ABC, abstractmethod

from words import SourceWord, ShadowWord, Sentence


class FileReader (ABC):

    def __init__(self):
        self._path = None

    @abstractmethod
    def read(self, path, type_code):
        pass

    @staticmethod
    @abstractmethod
    def extract_task_data(data):
        """Take the relevant data out of the imported data.

        Args:
            data: DataFrame of imported data

        Returns:
            extraction: DataFrame of only the relevant data
        """
        pass

    @staticmethod
    def df_to_words(df, type_code='source'):
        """Convert the rows in a DataFrame to Word instances.

        Args:
            df: DataFrame with the onset, offset and string of a Word
            type_code: Role of files ('source' or 'shadow')

        Returns:
            words: Sentence of converted Words
        """
        ws = []
        for row in df.itertuples(name='Words'):
            if type_code == 'source':
                word = SourceWord(row.Word.lower(), row.Onset, row.Offset)
            else:
                word = ShadowWord(row.Word.lower(), row.Onset, row.Offset)
            ws.append(word)
        words = Sentence(ws)
        return words

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, pth):
        self._path = pth


class CSVReader(FileReader):

    def __init__(self):
        super().__init__()

    def read(self, path, type_code):
        """Read the data into a workable format.

        Args:
            path: Path to data file
            type_code: Role of file ('source' or 'shadow')

        Returns:
            words: File contents converted to Word objects
        """
        if '.csv' not in path:
            pass
        else:
            df = pd.read_csv(path, header=None, sep='\n')
            df = df[0].str.split('\t', expand=True)
            data = self.extract_task_data(df)
            words = self.df_to_words(data, type_code)
            return words

    @staticmethod
    def extract_task_data(data):
        """Take the relevant data out of the already imported .csv
        data.

        Args:
            data: DataFrame of imported .csv data

        Returns:
            extraction: DataFrame of only the relevant data
        """
        extraction = data.iloc[:, 5:8]
        extraction = extraction.copy()
        extraction.columns = ['Onset', 'Offset', 'Word']
        extraction[['Onset', 'Offset']] = (extraction[['Onset', 'Offset']]
                                           .apply(pd.to_numeric))
        return extraction


class FileWriter(ABC):

    @abstractmethod
    def write_frame(self, path, results, result_type):
        pass

    @abstractmethod
    def _participant_path(self, directory, number):
        pass


class CSVWriter(FileWriter):

    def __init__(self):
        super().__init__()

    @staticmethod
    def write_frame(path, results, result_type):
        """Write results of given type to appropriate file.

        Args:
            path: Path to save location
            results: Analysis results to write to file
        """
        results.sort_values(by=['participant', 'video', 'condition'],
                            inplace=True)
        results.to_csv(path + '/' + result_type)

    @staticmethod
    def write_frame(path, results, result_type):
        """Write results of given type to appropriate file.

        Args:
            path: Path to save location
            results: Results to write to file
            result_type: Type of the results ('mistakes.csv' or delay.csv')
        """
        results.sort_values(by=['participant', 'video', 'condition'],
                            inplace=True)
        results.to_csv(path + '/' + result_type)

    def _participant_path(self, directory, number):
        """Create a path for the appropriate participant.

        Args:
            directory: Directory of the save location
            number: The participant's unique number

        Returns:
            pth: Path to participant save location
        """
        pth = directory + '/' + number
        return pth
