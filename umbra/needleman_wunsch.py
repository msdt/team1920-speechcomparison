import numpy as np
from alignment_strategy import AlignmentStrategy
from mistake_enum import Mistake


class NeedlemanWunsch(AlignmentStrategy):
    """Class that aligns the words of a source and a shadow Sentence
    using the Needleman-Wunsch algorithm."""

    def __init__(self, seman_checker, form_checker, phon_checker):
        super().__init__()
        self._match = 4
        self._mismatch = -2
        self._gap_sc = -1
        self._seman_match = 2
        self._phon_match = 2
        self._repetition = 0
        self._form_match = 2
        self._pointers = ['diag', 'up', 'left']
        self._source = None
        self._shadow = None
        self._matrix = None
        self._seman_checker = seman_checker
        self._form_checker = form_checker
        self._phon_checker = phon_checker

    def __str__(self):
        """Provide a String that represents the Needleman-Wunsch
        algorithm.

        Returns:
            The String literal "nw"
        """
        return "nw"

    def align(self, source, shadow):
        """Align a pair of source and shadow Sentences using the
        Needleman-Wunsch algorithm.

        Args:
            source: The Sentence of SourceWords
            shadow: The Sentence of ShadowWords
        """
        self._source = source
        self._shadow = shadow
        self._initialize_matrix()
        self._fill_matrix()
        self._traceback()

    def alignment_options(self, match=None, mismatch=None,
                          gap_sc=None, seman_match=None, phon_match=None,
                          repetition=None, form_match=None):
        """Set the scores that are allocated whilst aligning.
        Can be changed one at a time or more at once.

        Args:
             match: The score that is allocated when two words are equal
             mismatch: The score that is allocated when two words do
             not match in any way
             gap_sc: The score that is allocated for a gap
             seman_match: The score that is allocated when two words
             align by virtue of semantic equivalence
             phon_match: The score that is allocated when two words
             sound the same
             repetition: The score that is allocated when a shadow word
             is a stuttering
             form_match: The score that is allocated when two words
             align by virtue of form equivalence
        """
        if match:
            self._match = match
        if mismatch:
            self._mismatch = mismatch
        if gap_sc:
            self._gap_sc = gap_sc
        if seman_match:
            self._seman_match = seman_match
        if phon_match:
            self._phon_match = phon_match
        if repetition:
            self._repetition = repetition
        if form_match:
            self._form_match = form_match

    def _initialize_matrix(self):
        """Initialize matrix according to the Needleman-Wunsch algorithm.
        The matrix will contain values and pointers, the latter indicating
        what shift in the matrix we take (up, up-left or left).
        The matrix that results will have the default values.
        """
        n = len(self._source)
        m = len(self._shadow)
        self._matrix = np.array(
            [[{'value': self._gap_sc * y, 'pointer': 'up'} if x == 0
              else {'value': self._gap_sc * x, 'pointer': 'left'} if y == 0
              else {'value': 0, 'pointer': 'diag'}
              for x in range(n + 1)] for y in range(m + 1)])

    def _fill_matrix(self):
        """Update the matrix according to the Needleman-Wunsch algorithm based
        on the source and shadow Sentences.
        """
        n = len(self._source)
        m = len(self._shadow)
        for i in range(1, m + 1):
            shadow_word = self._shadow[i - 1]
            for j in range(1, n + 1):
                source_word = self._source[j - 1]
                if (source_word == shadow_word
                        and source_word.onset < shadow_word.onset):
                    value = self._match
                elif self._form_checker.related(self._source[j - 1].word,
                                                self._shadow[i - 1].word):
                    value = self._form_match
                elif self._seman_checker.related(source_word.word,
                                                 shadow_word.word):
                    value = self._seman_match
                elif self._phon_checker.related(source_word.word,
                                                shadow_word.word):
                    value = self._phon_match
                else:
                    value = self._mismatch
                match_value = self._matrix[i - 1, j - 1]['value'] + value

                delete = self._matrix[i - 1, j]['value']
                if self._check_repetition(i - 1):
                    delete += self._repetition
                else:
                    delete += self._gap_sc

                insert = self._matrix[i, j - 1]['value'] + self._gap_sc
                max_value = max([match_value, delete, insert])
                self._matrix[i, j]['value'] = max_value
                self._matrix[i, j]['pointer'] = self._pointers[
                    np.argmax([match_value, delete, insert]).item()]

    def _traceback(self):
        """Find the optimal alignment by tracing back to the top left."""
        j = len(self._source)
        i = len(self._shadow)
        while i > 0 or j > 0:
            if self._matrix[i][j]['pointer'] == 'diag':
                self._source[j - 1], self._shadow[i - 1] = \
                    self._equals_checker(self._source[j - 1],
                                         self._shadow[i - 1])
                i -= 1
                j -= 1

            elif self._matrix[i][j]['pointer'] == 'left':
                self._source[j - 1].mistake = Mistake.SKIPPED
                j -= 1

            elif self._matrix[i][j]['pointer'] == 'up':
                self._shadow[i - 1].mistake = Mistake.RANDOM
                if self._check_repetition(i - 1):
                    self._shadow[i - 1].mistake = Mistake.REPETITION
                i -= 1

    def _check_repetition(self, index):
        """Check whether the ShadowWord at the given index is repeated.

        Args:
            index: The index of the ShadowWord to check

        Returns:
            Boolean indicating whether the word at index is a repetition of
            a nearby ShadowWord
        """
        word = self._shadow[index]

        # Look back until the start of the Sentence
        for back in range(1, index + 1):
            if (self._shadow[index - back].word.endswith(word.word)
                    and self._exists_repeated_interval(index, back)):
                return True

        # Test whether the next word starts with the current
        return (index < len(self._shadow) - 1
                and self._shadow[index + 1].word.startswith(word.word))

    def _exists_repeated_interval(self, index, size):
        """Check whether there exists an interval of the specified size
        around the shadow word at the given index in which all shadow
        words are repetitions of words 'size' places back.

        Args:
            index: The index of the central ShadowWord
            size: The size of the interval, also the distance between
            Words that are compared

        Returns:
            Boolean indicating whether such an interval exists
        """
        assert index >= size
        for begin in range(index - size + 1, index + 1):
            # Check boundaries to prevent index out of bounds errors
            if begin + size > len(self._shadow) or begin < size:
                continue
            # Test the current interval for repetition
            elif all([early.endswith(late) for late, early in zip(
                   [word.word for word in self._shadow[begin:begin + size]],
                   [word.word for word in self._shadow[begin - size:begin]])]):
                return True
        return False

    def _equals_checker(self, source, shadow):
        """Check whether two Words are syntactically, semantically, form-wise
         or phonetically equivalent and set their parameters accordingly.

        Args:
            source: The SourceWord to be compared
            shadow: The ShadowWord to be compared

        Returns:
            source: The SourceWord with updated parameters
            shadow: The ShadowWord with updated parameters
        """
        if source == shadow and source.onset < shadow.onset:
            source.shadowed = True
            shadow.correct = True
            source.shadow = shadow
        elif self._seman_checker.related(source.word, shadow.word):
            source.mistake = Mistake.SEMANTIC
            shadow.mistake = Mistake.SEMANTIC
            source.shadow = shadow
        elif self._form_checker.related(source.word, shadow.word):
            source.mistake = Mistake.FORM
            shadow.mistake = Mistake.FORM
            source.shadow = shadow
        elif self._phon_checker.related(source.word, shadow.word):
            source.mistake = Mistake.PHONETIC
            shadow.mistake = Mistake.PHONETIC
            source.shadow = shadow
        else:
            shadow.mistake = Mistake.RANDOM
        return source, shadow
