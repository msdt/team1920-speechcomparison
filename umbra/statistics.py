from anchor_algorithm import AnchorAlgorithm
from dutch_phonenetics import DutchPhonetics
from form_checker import FormChecker
from mistake_counter import MistakeCounter
from mistake_finder import MistakeFinder
from needleman_wunsch import NeedlemanWunsch
from semantic_checker import SemanticChecker
from utils import get_path


class Statistics:
    """A class for storing the mistake checkers and the currently used
    algorithm for analysis of the results. Is part of the model."""

    def __init__(self):
        self.path = get_path(
            'OpenDutchWordnet/resources/odwn/odwn_orbn_gwg-LMF_1.3.xml.gz')
        self._seman_checker = SemanticChecker()
        self._form_checker = FormChecker()
        self._phon_checker = DutchPhonetics()
        self._mistake_finder = MistakeFinder(self._seman_checker,
                                             self._form_checker,
                                             self._phon_checker)
        self._mistake_counter = MistakeCounter()
        self._strategy = AnchorAlgorithm(self._mistake_finder)
        self._last_used_strategy = None

    @property
    def mistake_counter(self):
        return self._mistake_counter

    @property
    def last_used_strategy(self):
        """Getter for the last used strategy, which is the strategy ran
        at the last alignment.

        Returns:
            last_used_strategy: String representation of the strategy
        """
        return self._last_used_strategy

    @property
    def strategy(self):
        """Getter for the strategy. Default is Anchor Algorithm.

        Returns:
            strategy: AlignmentStrategy class
        """
        return self._strategy

    @strategy.setter
    def strategy(self, strategy_type):
        """Setter for the strategy used for the alignment.

        Args:
            strategy_type: "anchor" to set strategy to Anchor algorithm and
            "nw" to set to Needleman-Wunsch algorithm. Otherwise, nothing
            is changed.
        """
        if strategy_type == 'anchor':
            self._strategy = AnchorAlgorithm(self._mistake_finder)
        if strategy_type == 'nw':
            self._strategy = NeedlemanWunsch(self._seman_checker,
                                             self._form_checker,
                                             self._phon_checker)

    def analyze(self, trial):
        """Analyze the given trial.

        Args:
            trial: Trial to analyze
        """
        self._last_used_strategy = str(self._strategy)
        source = trial.source
        shadow = trial.shadow

        self._strategy.align(source, shadow)

        trial.results = self._mistake_counter.calculate_accuracy(source,
                                                                 shadow)
        delays_per_word = []
        for source_word in source:
            if source_word.has_shadow():
                delays_per_word.append([trial.participant, trial.condition,
                                        trial.video, source_word.word,
                                        source_word.get_difference
                                        (source_word.shadow)])
        trial.delays = delays_per_word
