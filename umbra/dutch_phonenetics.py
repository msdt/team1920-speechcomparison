import itertools
import nltk


class DutchPhonetics:
    """"A class for translating the spelling of a word into a phonetic
    representation and analyzing such phonetic representations."""

    def __init__(self):
        self.unvoiced = {
            'P': 'p',
            'T': 't',
            'K': 'k',
            'F': 'f',
            'S': 's'}
        self.voiced = {
            'B': 'b',
            'D': 'd',
            'V': 'v',
            'Z': 'z',
            'G': 'g>',
            'H': 'h_',
            'L': 'l',
            'R': 'r',
            'M': 'm',
            'N': 'n',
            'W': 'v_'}
        self.devoicing = {
            'V': 'f',
            'Z': 's',
            'G': 'x',
            'D': 't'}
        self.revoicing = {
            'F': 'v',
            'S': 'z',
            'P': 'b',
            'T': 'd',
            'K': 'g'}
        self.vowels = {
            'A': 'a>',
            'E': 'e>',
            'I': 'i>',
            'O': 'o>',
            'U': 'y>'}
        self.long_vowels = {
            key: vowel + ':' for (key, vowel) in self.vowels.items()}
        self.double_diphthong = {
            'AA': 'a:',
            'EE': 'e:',
            'IE': 'i',
            'OO': 'o:',
            'OE': 'u'}
        self.double = {
            'UU': 'y',
            'EU': '2:',
            'OE': 'u',
            'UE': 'u:',
            'IJ': 'e>i',
            'EI': 'e>i',
            'UI': '9y',
            'OU': 'v>u',
            'AU': 'v>u',
            'SJ': 's>',
            'NG': 'n>',
            'DT': 't',
            'TH': 't'}
        self.triples = {
            'AAI': 'a:i',
            'OOI': 'o:i',
            'OEI': 'u:i'}
        self.quadruples = {
            'EEUW': 'e:u',
            'IEUW': 'i:u'}
        self.aigu = {
            'Á': 'a:',
            'É': 'e:',
            'Í': 'i',
            'Ó': 'o:',
            'Ú': 'y',
            'Ý': 'e>i'}
        self.grave_circondakje = {
            'À': 'a>',
            'È': 'e>',
            'Ì': 'i>',
            'Ò': 'o>',
            'Ù': 'y>',
            'Â': 'a>',
            'Ê': 'e>',
            'Î': 'i>',
            'Ô': 'o>',
            'Û': 'y>'}
        self.trema = {
            'Ë': 'e>',
            'Ï': 'i>'}
        self.umlaut = {
            'Ä': ('a>', 'e>'),
            'Ö': ('o>', '2:'),
            'Ü': ('y>', 'y')}
        self.other = {
            'Y': 'e>i',
            'Q': 'k'}
        self.ambiguous = {
            'CH': ('x', 'g>', 's>'),
            'J': ('z>', 'j', 'dz>'),
            'X': ('ks', 'z')}

    def related(self, word1, word2):
        """Checks whether word1 and word2 are phonetically similar.

        Args:
            word1: First Word to check
            word2: Second Word to check
        Returns:
            Whether word1 and word2 are phonetically similar.
        """
        word1_phon = PhonRep(word1)
        word2_phon = PhonRep(word2)
        self._convert_to_phone(word1_phon)
        self._convert_to_phone(word2_phon)
        combinations = list(itertools.product(word1_phon.representation,
                                              word2_phon.representation))
        for comb in combinations:
            rep_word1 = comb[0]
            rep_word2 = comb[1]
            return self.check_lengths(rep_word1, rep_word2, word1, word2)
        return False

    @staticmethod
    def check_lengths(rep_word1, rep_word2, word1, word2):
        """Check lengths of Words and their representations.

        Args:
            rep_word1: First word's phonetic representation
            rep_word2: Second word's phonetic representation
            word1: First word String
            word2: Second word String

        Returns:
            Whether the Words and their representations are
            of appropriate length
        """
        dis = nltk.edit_distance(rep_word1, rep_word2)
        return (dis <= len(rep_word2) / 2
                and dis <= len(rep_word1) / 2
                and (dis <= len(word1) / 2 or dis <= len(word2) / 2)
                and word1 != word2 and len(word1) > 3
                and len(word2) > 3)

    def _convert_to_phone(self, word):
        """Convert raw phonetic representation to correct one.

        Args:
            word: Phonetic Word representation to be converted

        Returns:
            Input Word converted to correct phonetic representation.
        """
        self.triple_letters(word)
        self.double_letters(word)
        self.single_letters(word)
        self.special_cases(word)
        return word

    def triple_letters(self, word):
        """Retrieve 3 letter patterns from input,
        and convert them to their phonetic representations.

        Args:
            word: Phonetic Word representation to be converted
        """
        exceptions = {'AIL': 'a>i', 'TSJ': 'ts>'}
        self.filter(word, exceptions, 3)

    def double_letters(self, word):
        """Retrieve 2 letter patterns from input,
        and convert them to their phonetic representations.

        Args:
            word: Phonetic Word representation to be converted
        """
        self.filter(word, self.triples, 3)
        self.filter(word, self.quadruples, 4)
        self.filter(word, self.double_diphthong, 2)
        self.filter(word, self.double, 2)
        self.filter(word, self.ambiguous, 2, ambiguous=True)

    def single_letters(self, word):
        """Convert single letters to their phonetic representations.

        Args:
            word: Phonetic Word representation to be converted
        """
        for l_idx, letter in enumerate(word.source):
            analyzed = self.analyze(letter, l_idx, word)
            if analyzed is not {}:
                self.filter(word, analyzed, 1)

    def special_cases(self, word):
        """Convert letters with diacritics to their phonetic
        representations.

        Args:
            word: Phonetic Word representation to be converted
        """
        self.filter(word, self.ambiguous, 1, ambiguous=True)
        self.filter(word, self.aigu, 1)
        self.filter(word, self.grave_circondakje, 1)
        self.filter(word, self.trema, 1)  # More common in Dutch
        self.filter(word, self.other, 1) # Less common in Dutch
        self.filter(word, self.umlaut, 1)

    def filter(self, word, symbols, length, ambiguous=False):
        """Filter letters from word and adapt phonetic representation
        accordingly.

        Args:
            word: Phonetic Word representation to be converted
            symbols: Dictionary of letter, sound pairs
            length: length of symbols in symbols

        Kwargs:
            ambiguous: Whether the sounds are 'ambiguous' and whether
                the representation should be updated accordingly
                (default: False)
        """
        for item in symbols:
            item_pos = word.source.find(item)
            while item_pos != -1:
                if item == 'E' and item_pos == len(word.source) - 2:
                    word.update('E', 'y>')
                if not ambiguous:
                    word.update(item, symbols[item])
                else:
                    word.update_ambiguous(item, symbols[item])
                word.source = (word.source[:item_pos] + '+'
                               + word.source[item_pos + length:])
                # '+' Indicates removal of recognized pattern
                item_pos = word.source.find(item)

    def analyze(self, letter, l_idx, word):
        """Determine with which category to filter and update the
        phonetic representation, based on letter.

        Args:
            letter: Letter to base category on.
            l_idx: Index of letter in word
            word: Word to filter phonetic representation of

        Returns:
            A dictionary containing phonetic representations of the
            category letter falls in.
        """
        unvoiced = self.unvoiced
        voiced = self.voiced
        revoicing = self.revoicing
        devoicing = self.devoicing
        vowels = self.vowels
        long_vowels = self.long_vowels
        if (letter in unvoiced and l_idx + 1 < len(word)
                and word.source[l_idx + 1] in voiced and letter in revoicing):
            return dict({letter: revoicing[letter]})

        if (letter in voiced and l_idx + 1 < len(word) and
                word.source[l_idx + 1] in unvoiced and letter in devoicing):
            return dict({letter: devoicing[letter]})

        if letter in unvoiced:
            return dict({letter: unvoiced[letter]})

        if (letter in voiced and l_idx == len(word.source) - 1
                and letter in devoicing):
            return dict({letter: devoicing[letter]})

        if letter in voiced:
            return dict({letter: voiced[letter]})

        if letter in vowels:
            # deal with all other vowels
            if l_idx + 2 < len(word):
                if ((word.source[l_idx + 1] in unvoiced
                     or word.source[l_idx + 1] in voiced)
                    and not (word.source[l_idx+2] in unvoiced
                             or word.source[l_idx+2] in voiced)):
                    return dict({letter: long_vowels[letter]})
                else:
                    return dict({letter: vowels[letter]})
            else:
                return dict({letter: vowels[letter]})
        return {}


class PhonRep:
    """A class for storing the possible phonetic representations
    of words.
    """

    def __init__(self, source):
        self._source = source.upper()
        self._representation = [self.source]

    def __len__(self):
        """Return the length of the source word."""
        return len(self._source)

    @property
    def source(self):
        return self._source

    @source.setter
    def source(self, src):
        self._source = src

    @property
    def representation(self):
        return self._representation

    @representation.setter
    def representation(self, rep):
        self._representation = rep

    def update(self, letter, sound):
        """Update the phonetic representation of the word.

        Args:
            letter: Normal representation of letter
            sound: Phonetic representation of letter to be added to the
                representation of the word
        """
        for rep_idx, rep in enumerate(self.representation):
            spel_len = len(letter)
            spelling_pos = rep.find(letter)

            if spelling_pos != -1:
                self.representation[rep_idx] = (rep[:spelling_pos]
                                                + sound
                                                + rep[spelling_pos
                                                      + spel_len:])

    def update_ambiguous(self, letter, sounds):
        """Update the phonetic representation of the word if it is
        ambiguous.

        Args:
            letter: Normal representation of letter
            sounds: Possible phonetic representations of letter
                to be updated
        """
        for amb_sound in sounds:
            self.update(letter, amb_sound)
