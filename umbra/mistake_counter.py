from mistake_enum import *



class MistakeCounter:

    def __init__(self):
        pass

    def _analyse_mistakes(self, source, shadow):
        """Count the number of mistakes of each type,
        made in the shadowing task.

        Args:
            source: List of SourceWords
            shadow: list of ShadowWords
            Errors and source/shadow counterparts
                have been assigned to all words

        Returns:
            repetition: Number of repetition mistakes
            phonetic: Number of phonetic mistakes
            semantic: Number of semantic mistakes
            skipped: Number of skipped words from source
            random: Number of not-aligned random mistakes in shadow
            form: Number of form mistakes in task
        """
        repetition = self.count_mistakes(shadow, Mistake.REPETITION)
        phonetic = self.count_mistakes(shadow, Mistake.PHONETIC)
        semantic = self.count_mistakes(shadow, Mistake.SEMANTIC)
        skipped = self.count_mistakes(source, Mistake.SKIPPED)
        random = self.count_mistakes(shadow, Mistake.RANDOM)
        form = self.count_mistakes(shadow, Mistake.FORM)
        return repetition, phonetic, semantic, skipped, random, form

    @staticmethod
    def count_mistakes(sentence, mistake):
        """Count the number of times the given mistake type
        is present in sentence.

        Args:
            sentence: Sentence of Words
            mistake: Type of mistake

        Returns:
            mistakes: Number of mistakes of given type
        """
        mistakes = 0
        for word in sentence:
            if word.mistake == mistake:
                mistakes += 1
        return mistakes

    def calculate_accuracy(self, source, shadow):
        """Calculate the accuracy of the shadow task,
        given by an aligned list of SourceWords and ShadowWords.

        Args:
            source: Sentence of SourceWords
            shadow: Sentence of ShadowWords
            Errors and source/shadow counterparts
                have been assigned to all words

        Returns:
            Number of mistakes, accuracy score and percentage of each
            mistake with regards to the total number of mistakes
        """
        repetition, phonetic, semantic, skipped, random, form = \
            self._analyse_mistakes(source, shadow)
        mistakes = repetition + phonetic + semantic + skipped + random + form
        accuracy = (len(source) - mistakes) / len(source)
        return ({'#mistakes': mistakes,
                 'accuracy': accuracy * 100,
                 '%phonetic': (phonetic / mistakes) * 100,
                 '%repetition': (repetition / mistakes) * 100,
                 '%form': (form / mistakes) * 100,
                 '%semantic': (semantic / mistakes) * 100,
                 '%skipped': (skipped / mistakes) * 100,
                 '%random': (random / mistakes) * 100})
